<?php

    class Projeto extends CI_Model {

        function get_projetos($id_cliente, $status){
          $this->db->select('*');
          $this->db->from('usuario_projeto');
          $this->db->join('projeto', 'projeto.id_projeto = usuario_projeto.id_projeto and 
                           projeto.status = "'.$status.'" and '.
                           'usuario_projeto.id_usuario = '.$id_cliente);

          $query_projetos = $this->db->get()->result_array();

          return $query_projetos;
        }

        function inserir_projeto($dados) {
          $this->db->insert('projeto', $dados);

          return $this->db->insert_id();
        }

        function adicionar_usuario($dados) {
          $this->db->where('id_usuario', $dados['id_usuario']);
          $this->db->where('id_projeto', $dados['id_projeto']);

          $query = $this->db->get('usuario_projeto');

          $query = $query->result_array();

          if(sizeof($query) == 0) $this->db->insert('usuario_projeto', $dados);
        }

        public function deletar_usuario_projeto($projeto, $usuario) {
            $this->db->where('id_usuario', $usuario);
            $this->db->where('id_projeto', $projeto);

            $this->db->delete('usuario_projeto');

        }

        function get_projeto($id) {
          $this->db->select('*');
          $this->db->from('projeto');
          $this->db->where('id_projeto', $id);
          $this->db->join('endereco', 'projeto.id_endereco = endereco.id_endereco');

          $query = array();
          $result_array = $this->db->get()->result_array();

          if (!empty($result_array)) {
            $query = $result_array[0];
          }          
          return $query;
        }

        function get_projeto_por_nome($nome) {
          $this->db->where("nome_projeto", $nome);
          $query = $this->db->get('projeto');
          $query = $query->result_array();
          return $query;
        }

        function editar_projeto($dados) {
          $this->db->where("id_projeto", $dados['id_projeto']);
          $this->db->update('projeto', $dados);
        }
    }

?>