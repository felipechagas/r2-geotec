<?php

    class Material extends CI_Model {

        function get_materiais() {
            $query = $this->db->get('material');

            $query = $query->result_array();

          return $query;
        }

        function get_materiais_gastos($id_gasto) {
            $this->db->select('*');
            $this->db->from('gasto_material');
            $this->db->where('gasto_material.id_gasto', $id_gasto);
            $this->db->join('material', 'material.id_material = gasto_material.id_material');

            $query = $this->db->get()->result_array();

            return $query;
        }

        function inserir_material($dados) {
            $query = $this->db->insert('material', $dados);

            return $this->db->insert_id();
        }

        function deletar_materiais_gasto($id_gasto) {
            $this->db->where('id_gasto', $id_gasto);
            $this->db->delete('gasto_material');
        }
    }

?>