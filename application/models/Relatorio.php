<?php

    class Relatorio extends CI_Model {

        function inserir_relatorio($dados) {
            $this->db->insert('relatorio', $dados);
        }

        function get_relatorios($id_projeto) {
            $this->db->where('id_projeto', $id_projeto);
            $this->db->order_by('data_relatorio', 'asc');

            $query = $this->db->get('relatorio')->result_array();

            return $query;
        }

        function get_relatorio($id) {
            $this->db->where('id_relatorio', $id);
            $query = $this->db->get('relatorio')->result_array();

            $query = $query[0];

            return $query;
        }

        function delete_relatorio($id_relatorio) {
            $this->db->where('id_relatorio', $id_relatorio);
            $this->db->delete('relatorio');
        }

        function update_relatorio($dados) {
            $this->db->where('id_relatorio', $dados['id_relatorio']);
            $this->db->update('relatorio', $dados);
        }

        function get_id_projeto($id) {
            try {
                $this->db->select('id_projeto');
                $this->db->from('relatorio');
                $this->db->where('id_relatorio', $id);

                $result_array = $this->db->get()->result_array();

                if (!empty($result_array)) {
                    $result_id = $result_array[0];
                    return $result_id["id_projeto"];
                }
              } catch (Exception $e) {
                log('error', 'ERRO SGBD - Relatorio Model - get_id_projeto ' . $e->getMessage());

                return -1;
              }
            return -1;
        }
    }

?>