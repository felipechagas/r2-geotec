<?php

    class Cronograma extends CI_Model {

        function inserir_cronograma($dados) {
            $this->db->insert('cronograma', $dados);
        }

        function get_cronograma($id_projeto) {
            $this->db->where('id_projeto', $id_projeto);
            $query = $this->db->get('cronograma');

            $query = $query->result_array();
            $query = $query[0];

          return $query;
        }

        function editar_cronograma($dados) {
            $this->db->where('id_projeto', $dados['id_projeto']);
            $this->db->update('cronograma', $dados);
        }
    }

?>