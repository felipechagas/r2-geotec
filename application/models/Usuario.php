<?php

    class Usuario extends CI_Model {

        function validate($usuario, $senha) {
          try {
            $where = "( REPLACE(usuario.email_login, '.', '')='$usuario' OR usuario.cpf_login='$usuario' OR usuario.cnpj_login='$usuario' ) AND usuario.senha='$senha'";

            $this->db->from('usuario');
            $this->db->where($where);

            return $this->db->get()->result_array();
          } catch (Exception $e) {
            log('error', 'ERRO SGBD - Usuario Model - validate ' . $e->getMessage());

            return 0;
          }
          return 0;
        }

        function inserir_usuario($dados) {
            $this->db->insert('usuario', $dados);
        }

        function editar_usuario($dados) {

            $this->db->where('id_usuario', $dados['id_usuario']);
            $this->db->update('usuario', $dados);
        }

        function editar_usuario_projeto($dados) {
            $this->db->where('id_usuario', $dados['id_usuario']);
            $this->db->where('id_projeto', $dados['id_projeto']);
            $this->db->update('usuario_projeto', $dados);
        }

        public function get_usuarios($tipo) {  
           $this->db->where('tipo', $tipo);
           $this->db->order_by('nome', 'asc');
           return $this->db->get('usuario')->result_array();
        }

        public function get_todos_usuarios() {
          $this->db->select('nome, id_usuario');
          $this->db->from('usuario');
          $this->db->where('tipo', 'cliente');
          $this->db->order_by('nome', 'asc');

          $query['clientes'] = $this->db->get()->result_array();

          $this->db->select('nome, id_usuario');
          $this->db->from('usuario');
          $this->db->where('tipo', 'cartorio');
          $this->db->order_by('nome', 'asc');

          $query['cartorios'] = $this->db->get()->result_array();

          $this->db->select('nome, id_usuario');
          $this->db->from('usuario');
          $this->db->where('tipo', 'prefeitura');
          $this->db->order_by('nome', 'asc');

          $query['prefeituras'] = $this->db->get()->result_array();
          
          return $query;
        }

        public function get_usuarios_projeto($id) {
          $this->db->select('nome, usuario_projeto.id_usuario, tipo, visivel');
          $this->db->from('usuario_projeto');
          $this->db->where('id_projeto', $id);
          $this->db->join('usuario', 'usuario.id_usuario = usuario_projeto.id_usuario');
          $this->db->order_by('nome', 'asc');

          $query = $this->db->get()->result_array();
          
          return $query;
        }

        public function get_usuario($id) {  
          $this->db->select('*');
          $this->db->from('endereco');
          $this->db->join('usuario', 'usuario.id_endereco = endereco.id_endereco');
          $this->db->where('id_usuario', $id);

          $query = $this->db->get()->result_array();
          $query = $query[0];
          
          return $query;
        }

        public function visibilidade($id_projeto, $id_usuario){
          $this->db->where('id_projeto', $id_projeto);
          $this->db->where('id_usuario', $id_usuario);
          $query = $this->db->get('usuario_projeto');

          $query = $query->result_array();
          return $query[0]['visivel'];
        }

        public function get_emails_usuarios_projeto($id_projeto) {
          try {
            $this->db->select('nome, email, tipo, visivel');
            $this->db->from('usuario_projeto');
            $this->db->where('id_projeto', $id_projeto);
            $this->db->join('usuario', 'usuario.id_usuario = usuario_projeto.id_usuario');
            $this->db->order_by('nome', 'asc');

            return $this->db->get()->result_array();
          } catch (Exception $e) {
            log('error', 'ERRO SGBD - Usuario Model - get_emails_usuarios_projeto ' . $e->getMessage());

            return array();
          }
          return array();
        }
    }

?>