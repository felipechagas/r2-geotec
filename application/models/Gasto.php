<?php

    class Gasto extends CI_Model {

        function inserir_gasto($dados) {
            $this->db->insert('gasto', $dados);
            return $this->db->insert_id();
        }

        function get_gastos($id_projeto) {
            $this->db->select('*');
            $this->db->from('gasto');
            $this->db->where('gasto.id_projeto', $id_projeto);

            $query = $this->db->get()->result_array();

            return $query;
        }

        function adicionar_material($dados) {
            $this->db->insert('gasto_material', $dados);
        }

        function deletar_gasto($id_gasto) {
            $this->db->where('id_gasto', $id_gasto);
            $this->db->delete('gasto');
        }
    }

?>