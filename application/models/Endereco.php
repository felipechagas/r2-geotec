<?php

    class Endereco extends CI_Model {

        function inserir_endereco($dados) {
            $this->db->insert('endereco', $dados);

            return $this->db->insert_id();
        }

        function editar_endereco($dados) {
        	$this->db->where('id_endereco', $dados['id_endereco']);
            $this->db->update('endereco', $dados);
        }
    }

?>