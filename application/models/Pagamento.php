<?php

    class Pagamento extends CI_Model {

        function inserir_pagamento($dados) {
            $this->db->insert('pagamento', $dados);
        }

        function get_pagamentos($id_projeto) {
            $this->db->where('id_projeto', $id_projeto);
            $this->db->order_by('data_pagamento', 'asc');

            $query = $this->db->get('pagamento')->result_array();

            return $query;
        }

        function get_pagamento($id) {
            $this->db->where('id_pagamento', $id);
            $query = $this->db->get('pagamento')->result_array();

            $query = $query[0];

            return $query;
        }

        function delete_pagamento($id_pagamento) {
            $this->db->where('id_pagamento', $id_pagamento);
            $this->db->delete('pagamento');
        }

        function update_pagamento($dados) {
            $this->db->where('id_pagamento', $dados['id_pagamento']);
            $this->db->update('pagamento', $dados);
        }
    }

?>