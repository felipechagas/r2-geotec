<?php

    class Acesso extends CI_Model {

        function adicionar_acesso($dados) {
          $this->db->insert('acesso', $dados);
        }

        function get_acessos($id_cliente) {
          $this->db->select('*');
          $this->db->from('acesso');
          $this->db->where('id_usuario', $id_cliente);
          $this->db->order_by('data_hora', 'desc');

          $query = $this->db->get()->result_array();

          return $query;
        }
    }

?>