<?php

    class Arquivo extends CI_Model {

        function insert_arquivo($dados) {
        	$this->db->insert('arquivo', $dados);
        }

        function get_arquivos($id_projeto) {
        	$this->db->select('id_arquivo,id_projeto,formato,nome,tipo');
            $this->db->from('arquivo');
            $this->db->where('id_projeto', $id_projeto);
            $this->db->order_by('nome', 'asc');

        	$query = $this->db->get();
        	$query = $query->result_array();
            $arquivos = array();

            for ($i=0; isset($query[$i]); $i++) {
                $arquivos[$query[$i]['formato']][$i] = $query[$i];
            }
        	
        	return $arquivos;
        }

        function get_arquivos_com_caminho($id_projeto) {
            $this->db->select('id_arquivo,id_projeto,formato,nome,tipo,endereco');
            $this->db->from('arquivo');
            $this->db->where('id_projeto', $id_projeto);
            $this->db->order_by('nome', 'asc');

            $query = $this->db->get();
            $query = $query->result_array();
            $arquivos = array();

            for ($i=0; isset($query[$i]); $i++) {
                $arquivos[$query[$i]['formato']][$i] = $query[$i];
            }
            
            return $arquivos;
        }

        function get_arquivo_por_nome($nome) {
        	$this->db->where("nome", $nome);
        	$query = $this->db->get('arquivo');
        	$query = $query->result_array();
        	return $query;
        }

        function get_arquivo($id) {
            $this->db->where("id_arquivo", $id);
            $query = $this->db->get('arquivo');
            $query = $query->result_array();
            $query = $query[0];
            return $query;
        }

        function get_arquivos_usuario($id_arquivo, $id_projeto){
            $this->db->select('*');
            $this->db->from('usuario_projeto');
            $this->db->where('id_arquivo', $id_arquivo);
            $this->db->where('usuario_projeto.id_usuario', $_SESSION['id_usuario']);
            $this->db->join('arquivo', 'usuario_projeto.id_projeto = arquivo.id_projeto');

            $query = $this->db->get();
            $query = $query->result_array();
            
            return $query;
        }

        function delete_arquivo($id) {
            $this->db->where('id_arquivo', $id);
            $this->db->delete('arquivo');
        }
    }

?>