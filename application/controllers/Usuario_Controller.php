<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Usuario_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');

        date_default_timezone_set('America/Fortaleza');
    }

    public function index() {
        
    }

    public function adicionar($tipo) {
        if(!$this->session->userdata('logged_in') || ( !($this->session->userdata('nivel_usuario') == "administrador")
            && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());

        $tipo_usuario['tipo'] = $tipo;

        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('email', 'E-mail', 'required');
        $this->form_validation->set_rules('cpf', 'CPF', 'callback_verificar_cpf');
        $this->form_validation->set_rules('cnpj', 'CNPJ', 'callback_verificar_cnpj');

        $this->form_validation->set_error_delimiters('<div class="container-error row">', '</div>');
        $this->form_validation->set_message('required','O campo "%s" é obrigatório');       

        if ($this->form_validation->run()) {
            $this->adicionar_usuario($tipo);
        } else {
            $this->load->view('template/header');
            $this->load->view('template/menu');
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_usuarios');
            $this->load->view('formularios/adicionar_usuario', $tipo_usuario);
            $this->load->view('template/footer');
        }
    }

    public function adicionar_usuario($tipo) {
        $dados['usuario']['nome'] = ucwords(mb_strtolower($this->input->post('nome'), 'UTF-8'));
        $dados['usuario']['senha'] = md5($this->input->post('email'));
        $dados['usuario']['email'] = $this->input->post('email');
        $dados['usuario']['email_login'] = $this->input->post('email');
        $dados['usuario']['email_login'] = str_replace('.', '', $dados['usuario']['email_login']);
        $dados['usuario']['email_login'] = str_replace('-', '', $dados['usuario']['email_login']);
        $dados['usuario']['email_login'] = str_replace('/', '', $dados['usuario']['email_login']);
        $dados['usuario']['email_login'] = str_replace('\\', '', $dados['usuario']['email_login']);
        $dados['usuario']['cpf'] = $this->input->post('cpf');
        $dados['usuario']['cpf_login'] = $this->input->post('cpf');
        $dados['usuario']['cpf_login'] = str_replace('.', '', $dados['usuario']['cpf_login']);
        $dados['usuario']['cpf_login'] = str_replace('-', '', $dados['usuario']['cpf_login']);
        $dados['usuario']['cpf_login'] = str_replace('/', '', $dados['usuario']['cpf_login']);
        $dados['usuario']['cpf_login'] = str_replace('\\', '', $dados['usuario']['cpf_login']);
        $dados['usuario']['cnpj'] = $this->input->post('cnpj');
        $dados['usuario']['cnpj_login'] = $this->input->post('cnpj');
        $dados['usuario']['cnpj_login'] = str_replace('.', '', $dados['usuario']['cnpj_login']);
        $dados['usuario']['cnpj_login'] = str_replace('-', '', $dados['usuario']['cnpj_login']);
        $dados['usuario']['cnpj_login'] = str_replace('/', '', $dados['usuario']['cnpj_login']);
        $dados['usuario']['cnpj_login'] = str_replace('\\', '', $dados['usuario']['cnpj_login']);
        $dados['usuario']['telefone_1'] = $this->input->post('telefone_1');
        $dados['usuario']['telefone_2'] = $this->input->post('telefone_2');
        $dados['usuario']['tipo'] = $tipo;

        $dados['endereco']['logradouro'] = $this->input->post('logradouro');
        $dados['endereco']['numero'] = $this->input->post('numero');
        $dados['endereco']['complemento'] = $this->input->post('complemento');
        $dados['endereco']['cidade'] = $this->input->post('cidade');
        $dados['endereco']['estado'] = $this->input->post('estado');
        $dados['endereco']['cep'] = $this->input->post('cep');
        $dados['endereco']['bairro'] = $this->input->post('bairro');

        $dados['usuario']['id_endereco'] = $this->endereco->inserir_endereco($dados['endereco']);

        $this->usuario->inserir_usuario($dados['usuario']);

        redirect(base_url('area-adm/'.$_SESSION['nivel_usuario'].'/'.$tipo));
    }

    function verificar_cpf() {
        $this->form_validation->set_message('verificar_cpf', 'CPF inválido.');

        $cpf = $this->input->post('cpf');

        if(empty($cpf)) {
            return true;
        }
     
        $cpf = str_replace('.', '', $cpf);
        $cpf = str_replace('-', '', $cpf);

        if (strlen($cpf) != 11) {
            return false;
        } else {   
             
            for ($t = 9; $t < 11; $t++) {
                 
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }
     
            return true;
        }   
    }

    function verificar_cnpj() {
        $this->form_validation->set_message('verificar_cnpj', 'CNPJ inválido.');

        $cnpj = $this->input->post('cnpj');

        if(empty($cnpj)) {
            return true;
        }

        $cnpj = preg_replace( '/[^0-9]/', '', $cnpj );
        
        $cnpj = (string)$cnpj;
        
        $cnpj_original = $cnpj;
        
        $primeiros_numeros_cnpj = substr( $cnpj, 0, 12 );
        
        if ( ! function_exists('multiplica_cnpj') ) {
            function multiplica_cnpj( $cnpj, $posicao = 5 ) {
                
                $calculo = 0;
                
                for ( $i = 0; $i < strlen( $cnpj ); $i++ ) {

                    $calculo = $calculo + ( $cnpj[$i] * $posicao );
                    
                    $posicao--;
                    
                    if ( $posicao < 2 ) {
                        $posicao = 9;
                    }
                }
                return $calculo;
            }
        }
        
        $primeiro_calculo = multiplica_cnpj( $primeiros_numeros_cnpj );
        
        $primeiro_digito = ( $primeiro_calculo % 11 ) < 2 ? 0 :  11 - ( $primeiro_calculo % 11 );
        
        $primeiros_numeros_cnpj .= $primeiro_digito;

        $segundo_calculo = multiplica_cnpj( $primeiros_numeros_cnpj, 6 );
        $segundo_digito = ( $segundo_calculo % 11 ) < 2 ? 0 :  11 - ( $segundo_calculo % 11 );
        
        $cnpj = $primeiros_numeros_cnpj . $segundo_digito;
        
        if ( $cnpj === $cnpj_original ) {
            return true;
        } else {
            return false;
        }
    }

    public function editar($id_usuario) {
        if(!$this->session->userdata('logged_in') || ( !($this->session->userdata('nivel_usuario') == "administrador")
            && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());

        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('email', 'E-mail', 'required');
        $this->form_validation->set_rules('cpf', 'CPF', 'callback_verificar_cpf');
        $this->form_validation->set_rules('cnpj', 'CNPJ', 'callback_verificar_cnpj');

        $this->form_validation->set_error_delimiters('<div class="container-error row">', '</div>');
        $this->form_validation->set_message('required','O campo "%s" é obrigatório');      

        $dados['usuario'] = $this->usuario->get_usuario($id_usuario);

        $dados['id_usuario'] = $id_usuario;
        if ($this->form_validation->run()) {
            $this->editar_usuario($dados['usuario']);
        } else {
            $post = $this->input->post();
            if($post == array()) {
                $post = $dados['usuario'];

                unset($post['id_endereco']);
                unset($post['id_usuario']);
                unset($post['senha']);
                unset($post['email_login']);
                unset($post['pais']);
                unset($post['tipo']);

                $_POST = $post;
            }
            
            $this->load->view('template/header');
            $this->load->view('template/menu');
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_usuarios');
            $this->load->view('formularios/editar_usuario', $dados);
            $this->load->view('template/footer');
        }
    }

    public function editar_usuario($dados_antigos) {
        $dados['usuario']['id_usuario'] = $dados_antigos['id_usuario'];
        $dados['usuario']['nome'] = ucwords(mb_strtolower($this->input->post('nome'), 'UTF-8'));
        $dados['usuario']['senha'] = md5($this->input->post('email'));
        $dados['usuario']['email'] = $this->input->post('email');
        $dados['usuario']['email_login'] = $this->input->post('email');
        $dados['usuario']['email_login'] = str_replace('.', '', $dados['usuario']['email_login']);
        $dados['usuario']['email_login'] = str_replace('-', '', $dados['usuario']['email_login']);
        $dados['usuario']['email_login'] = str_replace('/', '', $dados['usuario']['email_login']);
        $dados['usuario']['email_login'] = str_replace('\\', '', $dados['usuario']['email_login']);
        $dados['usuario']['cpf'] = $this->input->post('cpf');
        $dados['usuario']['cnpj'] = $this->input->post('cnpj');
        $dados['usuario']['telefone_1'] = $this->input->post('telefone_1');
        $dados['usuario']['telefone_2'] = $this->input->post('telefone_2');

        $dados['endereco']['id_endereco'] = $dados_antigos['id_endereco'];
        $dados['endereco']['logradouro'] = $this->input->post('logradouro');
        $dados['endereco']['numero'] = $this->input->post('numero');
        $dados['endereco']['complemento'] = $this->input->post('complemento');
        $dados['endereco']['cidade'] = $this->input->post('cidade');
        $dados['endereco']['estado'] = $this->input->post('estado');
        $dados['endereco']['cep'] = $this->input->post('cep');
        $dados['endereco']['bairro'] = $this->input->post('bairro');
        
        $this->endereco->editar_endereco($dados['endereco']);

        $this->usuario->editar_usuario($dados['usuario']);

        redirect(base_url('area-adm/'.$_SESSION['nivel_usuario'].'/andamento/'.$dados_antigos['id_usuario']));
    }

    public function deletar_usuario_projeto($projeto, $usuario) {
        if(!$this->session->userdata('logged_in') || ( !($this->session->userdata('nivel_usuario') == "administrador")
            && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());

        $this->projeto->deletar_usuario_projeto($projeto, $usuario);

        redirect(base_url('area-adm/'.$_SESSION['nivel_usuario'].'/arquivos/'.$projeto));
    }

    public function adicionar_usuario_projeto($id_projeto, $tipo) {
        if(!$this->session->userdata('logged_in') || ( !($this->session->userdata('nivel_usuario') == "administrador")
            && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());
        
        if($this->input->post('usuario_'.$tipo)!="") {
            $dados['id_projeto'] = $id_projeto;
            $dados['id_usuario'] = $this->input->post('usuario_'.$tipo);
            
            $this->projeto->adicionar_usuario($dados);
        }

        redirect(base_url('area-adm/'.$_SESSION['nivel_usuario'].'/arquivos/'.$id_projeto));
    }

    public function visibilidade($id_projeto, $id_usuario, $visivel) {
        if($visivel) $visivel = 0;
        else $visivel = 1;

        $dados['id_projeto'] = $id_projeto;
        $dados['id_usuario'] = $id_usuario;
        $dados['visivel'] = $visivel;

        $this->usuario->editar_usuario_projeto($dados);

        redirect(base_url('area-adm/'.$_SESSION['nivel_usuario'].'/arquivos/'.$id_projeto));
    }
}

?>