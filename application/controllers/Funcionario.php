<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Funcionario extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');

        date_default_timezone_set('America/Fortaleza');
    }

    public function index() {
        
    }

    public function listar_usuarios($tipo_cliente) {
        if(!$this->session->userdata('logged_in') || !($this->session->userdata('nivel_usuario') == "funcionario"))
            redirect(base_url());

        $usuarios['usuarios'] = $this->usuario->get_usuarios($tipo_cliente);
        $usuarios['tipo'] = $tipo_cliente;

        for ($i = 0; isset($usuarios['usuarios'][$i]); $i++) {
            $usuarios['usuarios'][$i]['numero_projetos_parados'] = count($this->projeto->get_projetos($usuarios['usuarios'][$i]['id_usuario'], 'parado'));
            $usuarios['usuarios'][$i]['numero_projetos_andamentos'] = count($this->projeto->get_projetos($usuarios['usuarios'][$i]['id_usuario'], 'andamento'));
            $usuarios['usuarios'][$i]['numero_projetos_concluidos'] = count($this->projeto->get_projetos($usuarios['usuarios'][$i]['id_usuario'], 'concluido'));
        }

        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_usuarios');
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/lista_usuarios', $usuarios);
        $this->load->view('template/footer');
    }

    public function listar_projetos($status, $id) {
        if(!$this->session->userdata('logged_in') || !($this->session->userdata('nivel_usuario') == "funcionario"))
            redirect(base_url());

        $projetos['projetos'] = $this->projeto->get_projetos($id, $status);
        
        $projetos['projetos_andamento'] = count($this->projeto->get_projetos($id, 'andamento'));
        $projetos['projetos_parados'] = count($this->projeto->get_projetos($id, 'parado'));
        $projetos['projetos_concluidos'] = count($this->projeto->get_projetos($id, 'concluido'));

        $projetos['usuario'] = $this->usuario->get_usuario($id);

        $projetos['acessos'] = $this->acesso->get_acessos($id);

        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_usuario', $projetos);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projetos', $projetos);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/listar_projetos', $projetos);
        $this->load->view('template/footer');
    }

    public function listar_arquivos($id) {
        if(!$this->session->userdata('logged_in') || !($this->session->userdata('nivel_usuario') == "funcionario"))
            redirect(base_url());

        $projeto['arquivos_por_formato'] = $this->arquivo->get_arquivos($id);
        $projeto['projeto'] = $this->projeto->get_projeto($id);
        $projeto['usuarios'] = $this->usuario->get_usuarios_projeto($id);
        $projeto['clientes'] = $this->usuario->get_usuarios('cliente');
        $projeto['prefeituras'] = $this->usuario->get_usuarios('prefeitura');
        $projeto['cartorios'] = $this->usuario->get_usuarios('cartorio');

        $projeto['projeto']['caminho_arquivos'] = "";

        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_projeto', $projeto);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projeto', $projeto);
        $this->load->view('formularios/adicionar_arquivo', $projeto);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/listar_arquivos', $projeto);
        $this->load->view('template/footer');
    }

    public function listar_historico($id) {
        if(!$this->session->userdata('logged_in') || (!($this->session->userdata('nivel_usuario') == "funcionario") && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());

        $historico['projeto'] = $this->projeto->get_projeto($id);
        $historico['usuarios'] = $this->usuario->get_usuarios_projeto($id);
        $historico['relatorios'] = $this->relatorio->get_relatorios($id);
        $historico['clientes'] = $this->usuario->get_usuarios('cliente');
        $historico['prefeituras'] = $this->usuario->get_usuarios('prefeitura');
        $historico['cartorios'] = $this->usuario->get_usuarios('cartorio');
        
        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_projeto', $historico);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projeto', $historico);
        $this->load->view('formularios/adicionar_relatorio', $historico);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/listar_relatorios', $historico);
        $this->load->view('template/footer');
    }

    public function listar_gastos($id) {
        if(!$this->session->userdata('logged_in') || (!($this->session->userdata('nivel_usuario') == "funcionario") && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());

        $gasto['projeto'] = $this->projeto->get_projeto($id);
        $gasto['usuarios'] = $this->usuario->get_usuarios_projeto($id);
        $gasto['materiais'] = $this->material->get_materiais($id);
        $gasto['gastos'] = $this->gasto->get_gastos($id);
        $gasto['clientes'] = $this->usuario->get_usuarios('cliente');
        $gasto['prefeituras'] = $this->usuario->get_usuarios('prefeitura');
        $gasto['cartorios'] = $this->usuario->get_usuarios('cartorio');

        for ($i = 0; isset($gasto['gastos'][$i]); $i++) {
            $gasto['gastos'][$i]['materiais'] = $this->material->get_materiais_gastos($gasto['gastos'][$i]['id_gasto']);
            $gasto['gastos'][$i]['gasto_total'] = $this->gasto_total($gasto['gastos'][$i]);
        }
        
        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_projeto', $gasto);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projeto', $gasto);
        $this->load->view('formularios/adicionar_gasto', $gasto);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/listar_gastos', $gasto);
        $this->load->view('template/footer');
    }

    function gasto_total($gasto) {
        $gasto_total = ($gasto['distancia']);
        $gasto_total = $gasto_total + $gasto['alimentacao'];
        $gasto_total = $gasto_total + $gasto['outros'];

        foreach ($gasto['materiais'] as $material) {
            $gasto_total = $gasto_total + $material['valor_hora']*$gasto['horas'];
        }

        return $gasto_total;
    }

    public function cronograma($id) {
        if(!$this->session->userdata('logged_in') || (!($this->session->userdata('nivel_usuario') == "funcionario") && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());

        $cronograma['projeto'] = $this->projeto->get_projeto($id);
        $cronograma['usuarios'] = $this->usuario->get_usuarios_projeto($id);
        $cronograma['cronograma'] = $this->cronograma->get_cronograma($id);
        $cronograma['clientes'] = $this->usuario->get_usuarios('cliente');
        $cronograma['prefeituras'] = $this->usuario->get_usuarios('prefeitura');
        $cronograma['cartorios'] = $this->usuario->get_usuarios('cartorio');
        
        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_projeto', $cronograma);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projeto', $cronograma);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/cronograma', $cronograma);
        $this->load->view('template/footer');
    }
}

?>