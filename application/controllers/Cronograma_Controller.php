<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Cronograma_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');

        date_default_timezone_set('America/Fortaleza');
    }

    public function index() {
        
    }

    public function editar($id) {
        if(!$this->session->userdata('logged_in') || ( !($this->session->userdata('nivel_usuario') == "administrador")
            && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());

        $cronograma['projeto'] = $this->projeto->get_projeto($id);
        $cronograma['usuarios'] = $this->usuario->get_usuarios_projeto($id);
        $cronograma['cronograma'] = $this->cronograma->get_cronograma($id);
        
        $this->form_validation->set_rules('previsao', 'Previsão', 'required|callback_verificar_data');

        $this->form_validation->set_error_delimiters('<div class="container-error row">', '</div>');
        $this->form_validation->set_message('required','Campo "%s" obrigatório');       

        if ($this->form_validation->run()) {

            $this->editar_cronograma($cronograma['cronograma']);
        } else {
            
        
            $this->load->view('template/header');
            $this->load->view('template/menu');
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_projeto', $cronograma);
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projeto', $cronograma);
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/cronograma', $cronograma);
            $this->load->view('template/footer');
        }
    }

    function concluir($id) {
        $cronograma = $this->cronograma->get_cronograma($id);
        $cronograma['data_entrega'] = date('Y-m-d h:m:s');

        $projeto['id_projeto'] =  $cronograma['id_projeto'];
        $projeto['status'] = 'concluido';

        $this->cronograma->editar_cronograma($cronograma);
        $this->projeto->editar_projeto($projeto);

        redirect(base_url('area-adm/'.$_SESSION['nivel_usuario'].'/cronograma/'.$cronograma['id_projeto']));
    }

    function editar_cronograma($cronograma) {
        if(!$this->session->userdata('logged_in') || ( !($this->session->userdata('nivel_usuario') == "administrador")
            && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());
        
        $dateTime = DateTime::createFromFormat('d/m/Y', $this->input->post('previsao'), new DateTimeZone('America/Sao_Paulo'));
        $timestamp = $dateTime->getTimestamp();
        $cronograma['previsao'] = date('Y-m-d h:m:s', $timestamp);

        $this->cronograma->editar_cronograma($cronograma);

        redirect(base_url('area-adm/'.$_SESSION['nivel_usuario'].'/cronograma/'.$cronograma['id_projeto']));
    }

    function verificar_data() {
        $this->form_validation->set_message('verificar_data','Data inválida.');

        if(strlen($this->input->post('previsao')) != 10) {
            return false;
        }

        $data_split = explode("/", $this->input->post('previsao'));

        $res = checkdate($data_split[1], $data_split[0], $data_split[2]);

        if ($res == 1){
           return true;
        } else {
            $this->form_validation->set_message('verificar_data','Data inválida2.');
           return false;
        }
    }
}

?>