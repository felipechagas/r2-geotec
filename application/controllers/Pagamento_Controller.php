<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Pagamento_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');

        date_default_timezone_set('America/Fortaleza');
    }

    public function index() {
        
    }

    public function adicionar($id) {
        if(!$this->session->userdata('logged_in') || !($this->session->userdata('nivel_usuario') == "administrador"))
            redirect(base_url());

        $this->form_validation->set_rules('valor_recebido', 'Valor Recebido', 'required');
        $this->form_validation->set_rules('tipo_pagamento', 'Forma de Pagamento', 'required');

        $this->form_validation->set_error_delimiters('<div class="container-error row">', '</div>');
        $this->form_validation->set_message('required','O campo "%s" é obrigatório.');       

        if ($this->form_validation->run()) {
            $this->adicionar_pagamento($id);
        } else {
            $pagamento['projeto'] = $this->projeto->get_projeto($id);
            $pagamento['usuarios'] = $this->usuario->get_usuarios_projeto($id);
            $pagamento['pagamentos'] = $this->pagamento->get_pagamentos($id);

            $this->load->view('template/header');
            $this->load->view('template/menu');
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_projeto', $pagamento);
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projeto', $pagamento);
            $this->load->view('formularios/adicionar_pagamento', $pagamento);
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/listar_pagamentos', $pagamento);
            $this->load->view('template/footer');
        }
    }

    public function adicionar_pagamento($id) {
        $dados['id_projeto'] = $id;
        $dados['tipo_pagamento'] = $this->input->post('tipo_pagamento');
        $dados['valor_recebido'] = $this->input->post('valor_recebido');


        $this->pagamento->inserir_pagamento($dados);

        redirect(base_url('area-adm/adicionar-pagamentos/'.$id));
    }

    public function deletar($id) {
        if(!$this->session->userdata('logged_in') || !($this->session->userdata('nivel_usuario') == "administrador"))
            redirect(base_url());

        $query = $this->pagamento->get_pagamento($id);
        $this->pagamento->delete_pagamento($id);

        redirect(base_url('area-adm/adicionar-pagamentos/'.$query['id_projeto']));
    }

    public function editar($id) {
        if(!$this->session->userdata('logged_in') || !($this->session->userdata('nivel_usuario') == "administrador"))
            redirect(base_url());

        $query = $this->pagamento->get_pagamento($id);

        $this->form_validation->set_rules('valor_recebido', 'Valor Recebido', 'required');
        $this->form_validation->set_rules('tipo_pagamento', 'Forma de Pagamento', 'required');

        $this->form_validation->set_error_delimiters('<div class="container-error row">', '</div>');
        $this->form_validation->set_message('required','O campo "%s" é obrigatório.');       

        if ($this->form_validation->run()) {
            $this->editar_pagamento($id, $query);
        } else {
            if($this->input->post('valor_recebido') == "" and $this->input->post('tipo_pagamento') == "") {
                $_POST['valor_recebido'] = $query['valor_recebido'];
                $_POST['tipo_pagamento'] = $query['tipo_pagamento'];
            }

            $pagamento['projeto'] = $this->projeto->get_projeto($query['id_projeto']);
            $pagamento['usuarios'] = $this->usuario->get_usuarios_projeto($query['id_projeto']);
            $pagamento['pagamento'] = $query;

            $this->load->view('template/header');
            $this->load->view('template/menu');
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_projeto', $pagamento);
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projeto', $pagamento);
            $this->load->view('formularios/editar_pagamento', $pagamento);
            $this->load->view('template/footer');
        }
    }

    public function editar_pagamento($id, $projeto){
        $dados['id_pagamento'] = $id;
        $dados['id_projeto'] = $projeto['id_projeto'];
        $dados['tipo_pagamento'] = $this->input->post('tipo_pagamento');
        $dados['valor_recebido'] = $this->input->post('valor_recebido');
        $dados['data_pagamento'] = $projeto['data_pagamento'];


        $this->pagamento->update_pagamento($dados);

        redirect(base_url('area-adm/adicionar-pagamentos/'.$projeto['id_projeto']));
    }
}

?>