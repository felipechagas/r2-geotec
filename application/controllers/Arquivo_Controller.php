<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Arquivo_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');

        date_default_timezone_set('America/Fortaleza');
    }

    public function index() {
        
    }

    public function adicionar($id) {
        if(!$this->session->userdata('logged_in') || ( !($this->session->userdata('nivel_usuario') == "administrador")
            && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());

        $projeto['projeto'] = $this->projeto->get_projeto($id);
        $projeto['arquivos_por_formato'] = $this->arquivo->get_arquivos($id);
        $projeto['usuarios'] = $this->usuario->get_usuarios_projeto($id);
        
        $this->form_validation->set_rules('nome', 'Nome', 'required|callback_verificar_nome');
        $this->form_validation->set_rules('arquivo', 'Arquivo', 'callback_verificar_upload');

        $this->form_validation->set_error_delimiters('<div class="container-error row">', '</div>');
        $this->form_validation->set_message('required','O campo "%s" é obrigatório');

        if ($this->form_validation->run()) {
            $this->adicionar_arquivo($id, $projeto['projeto']['caminho_arquivos']);
        } else {
            $this->load->view('template/header');
            $this->load->view('template/menu');
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_projeto', $projeto);
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projeto', $projeto);
            $this->load->view('formularios/adicionar_arquivo', $projeto);
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/listar_arquivos', $projeto);
            $this->load->view('template/footer');
        }
    }

    public function adicionar_arquivo($id, $caminho) {
        $arquivo = $_FILES['arquivo'];

        
        $dados['tipo'] = $this->input->post('tipo');
        $dados['nome'] = $this->input->post('nome');
        $dados['id_projeto'] = $id;
        $dados['formato'] = explode('.', $arquivo['name']);
        $dados['formato'] = $dados['formato'][sizeof($dados['formato'])-1];
        $dados['endereco'] = $caminho."/".nome_arquivo_upload($dados['nome'], $dados['formato']);

        $this->arquivo->insert_arquivo($dados);
        redirect(base_url('area-adm/'.$_SESSION['nivel_usuario'].'/arquivos/'.$id));
    }

    public function download($id_arquivo) {
        if(!$this->session->userdata('logged_in') ||
          ((!($this->session->userdata('nivel_usuario') == "administrador") &&
           !($this->session->userdata('nivel_usuario') == "funcionario")) &&
           !($this->verificar_usuario($id_arquivo))
          ))
            redirect(base_url());

        $query = $this->arquivo->get_arquivo($id_arquivo);

        $data = file_get_contents($query['endereco']);
        $nome = $query['nome'].".".$query['formato'];

        force_download($nome, $data);
    }

    public function verificar_usuario($id_arquivo) {
        $query = $this->arquivo->get_arquivo($id_arquivo);
        
        $query2 = $this->arquivo->get_arquivos_usuario($id_arquivo, $query['id_projeto']);

        return sizeof($query2)>0;
    }

    public function verificar_upload() {
        $id = $this->input->post('id');
        $projeto = $this->projeto->get_projeto($id);
        $caminho = $projeto['caminho_arquivos'];
        
        $arquivo = $_FILES['arquivo'];

        if($arquivo['size'] == 0) {
            $this->form_validation->set_message('verificar_upload', 'Faça upload de um arquivo.');

            return false;
        }

        $dados['tipo'] = $this->input->post('tipo');
        $dados['nome'] = $this->input->post('nome');
        $dados['id_projeto'] = $id;
        $dados['formato'] = explode('.', $arquivo['name']);
        $dados['formato'] = $dados['formato'][sizeof($dados['formato'])-1];
        $dados['endereco'] = $caminho."/".nome_arquivo_upload($dados['nome'], $dados['formato']);

        $file_name = nome_arquivo_upload($dados['nome'], $dados['formato']);
        $allowed_types = $dados['formato'];
        
        $configuracao = array(
            'upload_path'   => $caminho,
            'file_name'     => $file_name,
            'allowed_types' => $allowed_types
        );

        $this->load->library('upload');
        $this->upload->initialize($configuracao);

        if ($this->upload->do_upload('arquivo')) {
            return true;
        }
        else {
            $this->form_validation->set_message('verificar_upload', $this->upload->display_errors().' Tente compactar o arquivo em .zip ou .rar.');

            return false;
        }
    }

    public function verificar_nome() {
        $query = $this->arquivo->get_arquivo_por_nome($this->input->post('nome'));

        if(sizeof($query)!=0) {
            $this->form_validation->set_message('verificar_nome', 'Nome de arquivo já existe.');
            return false;
        } else {
            return true;
        }
    }

    public function deletar($id) {
        if(!$this->session->userdata('logged_in') || ( !($this->session->userdata('nivel_usuario') == "administrador")
            && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());

        $query = $this->arquivo->get_arquivo($id);
        unlink($query['endereco']);
        $this->arquivo->delete_arquivo($id);
        
        redirect(base_url('area-adm/adicionar-arquivos/'.$query['id_projeto']));
    }

    function download_todos($id_projeto) {
        if(!$this->session->userdata('logged_in') ||
          ((!($this->session->userdata('nivel_usuario') == "administrador") &&
           !($this->session->userdata('nivel_usuario') == "funcionario")) &&
           !($this->verificar_usuario_projeto($id_projeto))
          ))
            redirect(base_url());
        
        $dados['projeto'] = $this->projeto->get_projeto($id_projeto);
        $dados['arquivos'] = $this->arquivo->get_arquivos_com_caminho($id_projeto);
        

        foreach ($dados['arquivos'] as $arquivos) {
            foreach ($arquivos as $key => $arquivo) {
                $this->zip->add_data($arquivo['nome'].".".$arquivo['formato'], $arquivo['endereco']);
            }
        }

        $this->zip->download('Arquivos '.$dados['projeto']['nome_projeto'].'.zip');
    }

    public function verificar_usuario_projeto($id_projeto) {
        $usuarios = $this->usuario->get_usuarios_projeto($id_projeto);

        foreach($usuarios as $usuario) {
            if($usuario['id_usuario'] = $_SESSION['id_usuario']) return true;
        }

        return false;
    }
}

?>