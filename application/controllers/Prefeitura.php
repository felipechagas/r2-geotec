<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Prefeitura extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');

        date_default_timezone_set('America/Fortaleza');
    }

    public function index() {
        
    }

    public function listar_projetos($status, $id) {

        if(!$this->session->userdata('logged_in') || !($_SESSION['id_usuario'] == $id))
            redirect(base_url());

        $projetos['projetos'] = $this->projeto->get_projetos($id, $status);
        
        $projetos['projetos_andamento'] = count($this->projeto->get_projetos($id, 'andamento'));
        $projetos['projetos_parados'] = count($this->projeto->get_projetos($id, 'parado'));
        $projetos['projetos_concluidos'] = count($this->projeto->get_projetos($id, 'concluido'));

        $projetos['usuario'] = $this->usuario->get_usuario($id);

        $projetos['acessos'] = $this->acesso->get_acessos($id);

        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_usuario', $projetos);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projetos', $projetos);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/listar_projetos', $projetos);
        $this->load->view('template/footer');
    }

    public function listar_arquivos($id) {
        if(!$this->session->userdata('logged_in') ||  !($this->verificar_usuario_projeto($id)))
            redirect(base_url());
        
        $projeto['arquivos_por_formato'] = $this->arquivo->get_arquivos($id);
        $projeto['projeto'] = $this->projeto->get_projeto($id);
        $projeto['usuarios'] = $this->usuario->get_usuarios_projeto($id);
        $projeto['clientes'] = $this->usuario->get_usuarios('cliente');
        $projeto['prefeituras'] = $this->usuario->get_usuarios('prefeitura');
        $projeto['cartorios'] = $this->usuario->get_usuarios('cartorio');

        $projeto['visibilidade_arquivo'] = $this->usuario->visibilidade($id, $_SESSION['id_usuario']);;

        $projeto['projeto']['caminho_arquivos'] = "";
        
        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_projeto', $projeto);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projeto', $projeto);
        $this->load->view('formularios/adicionar_arquivo', $projeto);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/listar_arquivos', $projeto);
        $this->load->view('template/footer');
    }

    public function verificar_usuario_projeto($id_projeto) {
        $usuarios = $this->usuario->get_usuarios_projeto($id_projeto);

        foreach($usuarios as $usuario) {
            if($usuario['id_usuario'] = $_SESSION['id_usuario']) return true;
        }

        return false;
    }

    public function listar_historico($id) {
        if(!$this->session->userdata('logged_in') ||  !($this->verificar_usuario_projeto($id)))
            redirect(base_url());

        $historico['projeto'] = $this->projeto->get_projeto($id);
        $historico['usuarios'] = $this->usuario->get_usuarios_projeto($id);
        $historico['relatorios'] = $this->relatorio->get_relatorios($id);
        $historico['clientes'] = $this->usuario->get_usuarios('cliente');
        $historico['prefeituras'] = $this->usuario->get_usuarios('prefeitura');
        $historico['cartorios'] = $this->usuario->get_usuarios('cartorio');
        
        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_projeto', $historico);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projeto', $historico);
        $this->load->view('formularios/adicionar_relatorio', $historico);
        $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/listar_relatorios', $historico);
        $this->load->view('template/footer');
    }

    public function criar_senha($id) {
        if(!$this->session->userdata('logged_in') || !($_SESSION['id_usuario'] == $id))
            redirect(base_url());

        $dados['id'] = $id;

        $this->form_validation->set_rules('senha', 'Crie uma senha', 'required');
        $this->form_validation->set_rules('repetir_senha', 'Repita a senha', 'required|callback_verificar_senha');

        $this->form_validation->set_error_delimiters('<div class="container-error row">', '</div>');
        $this->form_validation->set_message('required','O campo "%s" é obrigatório');

        if ($this->form_validation->run()) {
            $this->salvar_senha($id);
        } else {
            $this->load->view('template/header');
            $this->load->view('template/menu');
            $this->load->view('formularios/criar_senha', $dados);
            $this->load->view('template/footer');
        }
    }

    function salvar_senha($id) {
        if(!$this->session->userdata('logged_in') || !($_SESSION['id_usuario'] == $id))
            redirect(base_url());

        $dados['id_usuario'] = $id;
        $dados['senha'] = md5($this->input->post('senha'));

        $this->usuario->editar_usuario($dados);

        $novo_acesso['id_usuario'] = $id;
        $this->acesso->adicionar_acesso($novo_acesso);

        redirect(base_url('area-adm/'.$_SESSION['nivel_usuario'].'/andamento/'.$_SESSION['id_usuario']));
    }

    public function verificar_senha() {
        $this->form_validation->set_message('verificar_senha','Senha não confere. Tenha certeza que digitou a mesma senha nos dois campos.');

        return $this->input->post('repetir_senha') == $this->input->post('senha');
    }
}

?>