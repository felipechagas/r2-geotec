<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Gasto_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');

        date_default_timezone_set('America/Fortaleza');
    }

    public function adicionar($id) {
        if(!$this->session->userdata('logged_in') || ( !($this->session->userdata('nivel_usuario') == "administrador")
            && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());


        $this->form_validation->set_rules('distancia', 'Distância', 'required');
        $this->form_validation->set_rules('alimentacao', 'Alimentação', 'required');
        $this->form_validation->set_rules('horas', 'Horas de Trabalho', 'required');
        $this->form_validation->set_rules('novo_material', 'Novo Material', 'callback_verificar_novo_material');

        $this->form_validation->set_error_delimiters('<div class="container-error row">', '</div>');
        $this->form_validation->set_message('required','O campo "%s" é obrigatório');       

        if ($this->form_validation->run()) {

            $this->adicionar_gasto($id);

        } else {
            $gasto['projeto'] = $this->projeto->get_projeto($id);
            $gasto['usuarios'] = $this->usuario->get_usuarios_projeto($id);
            $gasto['materiais'] = $this->material->get_materiais($id);
            $gasto['gastos'] = $this->gasto->get_gastos($id);

            for ($i = 0; isset($gasto['gastos'][$i]); $i++) {
                $gasto['gastos'][$i]['materiais'] = $this->material->get_materiais_gastos($gasto['gastos'][$i]['id_gasto']);
                $gasto['gastos'][$i]['gasto_total'] = $this->gasto_total($gasto['gastos'][$i]);
            }
        
            
            $this->load->view('template/header');
            $this->load->view('template/menu');
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_projeto', $gasto);
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projeto', $gasto);
            $this->load->view('formularios/adicionar_gasto', $gasto);
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/listar_gastos', $gasto);
            $this->load->view('template/footer');
        }
    }

    function gasto_total($gasto) {
        $gasto_total = $gasto['distancia'];
        $gasto_total = $gasto_total + $gasto['alimentacao'];
        $gasto_total = $gasto_total + $gasto['outros'];

        foreach ($gasto['materiais'] as $material) {
            $gasto_total = $gasto_total + $material['valor_hora']*$gasto['horas'];
        }

        return $gasto_total;
    }

    public function adicionar_gasto($id) {
        $dados['id_projeto'] = $id;
        $dados['distancia'] = $this->input->post('distancia');
        $dados['alimentacao'] = $this->input->post('alimentacao');
        $dados['horas'] = $this->input->post('horas');
        $dados['outros'] = $this->input->post('outros');

        $gastos_materiais['id_gasto'] = $this->gasto->inserir_gasto($dados);

        $materiais = $this->material->get_materiais($id);

        $materiais_usados = array();
        foreach ($materiais as $material) {
            if($this->input->post($material['id_material']) == "on") {
                $gastos_materiais['id_material'] = $material['id_material'];

                $this->gasto->adicionar_material($gastos_materiais);
            }
        }

        if($this->input->post('novo_material')!="" && $this->input->post('novo_valor')!="") {
            $material_dados['nome'] = $this->input->post('novo_material');
            $material_dados['valor_hora'] = $this->input->post('novo_valor');

            $gastos_materiais['id_material'] = $this->material->inserir_material($material_dados);
            $this->gasto->adicionar_material($gastos_materiais);
        }
        
        redirect(base_url('area-adm/'.$_SESSION['nivel_usuario'].'/gastos/'.$id));
    }

    public function verificar_novo_material() {
        $this->form_validation->set_message('verificar_novo_material','Novo material inválido.');  

        if($this->input->post('novo_material')=="" && $this->input->post('novo_valor')=="") {
            return true;
        } 

        if(($this->input->post('novo_material')=="" && $this->input->post('novo_valor')!="") || ($this->input->post('novo_material')!="" && $this->input->post('novo_valor')=="")) {
            $this->form_validation->set_message('verificar_novo_material','Ambos os campos devem ser preenchidos.');

            return false;
        }

        return true;
    }

    public function deletar($id_gasto) {
        if(!$this->session->userdata('logged_in') || !($this->session->userdata('nivel_usuario') == "administrador"))
            redirect(base_url());

        $this->material->deletar_materiais_gasto($id_gasto);
        $this->gasto->deletar_gasto($id_gasto);
        
        redirect(base_url('area-adm/'.$_SESSION['nivel_usuario'].'/gastos/'.$id));
    }
}

?>