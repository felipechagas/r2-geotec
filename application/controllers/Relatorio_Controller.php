<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Relatorio_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');

        date_default_timezone_set('America/Fortaleza');
    }

    public function index() {
        
    }

    public function adicionar($id) {
        if(!$this->session->userdata('logged_in') || ( !($this->session->userdata('nivel_usuario') == "administrador")
            && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());

        $this->form_validation->set_rules('descricao', 'Relatório', 'required');

        $this->form_validation->set_error_delimiters('<div class="container-error row">', '</div>');
        $this->form_validation->set_message('required','O campo "%s" é obrigatório.');       

        if ($this->form_validation->run()) {
            $this->adicionar_relatorio($id);
        } else {
            $pagamento['projeto'] = $this->projeto->get_projeto($id);
            $pagamento['usuarios'] = $this->usuario->get_usuarios_projeto($id);
            $pagamento['relatorios'] = $this->relatorio->get_relatorios($id);
            
            $this->load->view('template/header');
            $this->load->view('template/menu');
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_projeto', $pagamento);
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projeto', $pagamento);
            $this->load->view('formularios/adicionar_relatorio', $pagamento);
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/listar_relatorios', $pagamento);
            $this->load->view('template/footer');
        }
    }

    public function adicionar_relatorio($id) {
        $dados['id_projeto'] = $id;
        $dados['descricao'] = $this->input->post('descricao');

        if($this->input->post('compartilhado') == 'on') {
            $dados['compartilhado'] = "1";

            $projeto = $this->projeto->get_projeto($id);
            $emails_usuarios_projeto = $this->usuario->get_emails_usuarios_projeto($id);

            if (!empty($emails_usuarios_projeto)) {
                $emails = "";

                foreach ($emails_usuarios_projeto as $email_usuario_projeto) {
                    $emails .= $email_usuario_projeto['email'];
                    $emails .= ", ";
                }
                // RETIRADA DA ULTIMA VIRGULA
                $emails = substr($emails, 0, -2);

                send_clients_report($emails, $projeto['nome_projeto'], $this->input->post('descricao'));
            }
        }

        $this->relatorio->inserir_relatorio($dados);

        redirect(base_url('area-adm/adicionar-historico/'.$id));
    }

    public function deletar($id) {
        if(!$this->session->userdata('logged_in') || ( !($this->session->userdata('nivel_usuario') == "administrador")
            && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());

        $query = $this->relatorio->get_relatorio($id);
        $this->relatorio->delete_relatorio($id);

        redirect(base_url('area-adm/adicionar-historico/'.$query['id_projeto']));
    }

    public function editar($id, $compartilhado) {
        if(!$this->session->userdata('logged_in') || ( !($this->session->userdata('nivel_usuario') == "administrador")
            && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());

        $query = $this->relatorio->get_relatorio($id);
        $query['compartilhado'] = $compartilhado;

        $this->relatorio->update_relatorio($query);

        if ($compartilhado) {
            $id_projeto = $this->relatorio->get_id_projeto($id);
            $projeto = $this->projeto->get_projeto($id_projeto);
            $emails_usuarios_projeto = $this->usuario->get_emails_usuarios_projeto($id_projeto);

            if (!empty($emails_usuarios_projeto)) {
                $emails = "";

                foreach ($emails_usuarios_projeto as $email_usuario_projeto) {
                    $emails .= $email_usuario_projeto['email'];
                    $emails .= ", ";
                }
                // RETIRADA DA ULTIMA VIRGULA
                $emails = substr($emails, 0, -2);

                send_clients_report($emails, $projeto['nome_projeto'], $query['descricao']);
            }
        }

        redirect(base_url('area-adm/adicionar-historico/'.$query['id_projeto']));
    }
}

?>