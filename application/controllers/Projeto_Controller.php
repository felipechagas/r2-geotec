<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Projeto_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');

        date_default_timezone_set('America/Fortaleza');
    }

    public function index() {
        
    }

    public function adicionar($id) {
        if(!$this->session->userdata('logged_in') || ( !($this->session->userdata('nivel_usuario') == "administrador")
            && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());

        $projetos['projetos_andamento'] = count($this->projeto->get_projetos($id, 'andamento'));
        $projetos['projetos_parados'] = count($this->projeto->get_projetos($id, 'parado'));
        $projetos['projetos_concluidos'] = count($this->projeto->get_projetos($id, 'concluido'));

        $projetos['usuario'] = $this->usuario->get_usuario($id);
        $projetos['acessos'] = $this->acesso->get_acessos($id);
        $projetos['usuarios'] = $this->usuario->get_todos_usuarios();

        $this->form_validation->set_rules('nome_projeto', 'Nome do Projeto', 'required|callback_verificar_nome');
        $this->form_validation->set_rules('descricao_projeto', 'Descrição', 'required');

        $this->form_validation->set_error_delimiters('<div class="container-error row">', '</div>');
        $this->form_validation->set_message('required','O campo "%s" é obrigatório');       

        if ($this->form_validation->run()) {
            $this->adicionar_projeto($id);
        } else {
            $this->load->view('template/header');
            $this->load->view('template/menu');
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/dados_usuario', $projetos);
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projetos', $projetos);
            $this->load->view('formularios/adicionar_projeto');
            $this->load->view('template/footer');
        }
    }

    public function adicionar_projeto($id) {
        $dados['projeto']['nome_projeto'] = ucwords(mb_strtolower($this->input->post('nome_projeto'), 'UTF-8'));
        $dados['projeto']['descricao_projeto'] = $this->input->post('descricao_projeto');
        $dados['projeto']['status'] = $this->input->post('status');
        $dados['projeto']['caminho_arquivos'] = criar_pasta_upload($id, $dados['projeto']['nome_projeto']);

        $dados['endereco']['logradouro'] = $this->input->post('logradouro');
        $dados['endereco']['numero'] = $this->input->post('numero');
        $dados['endereco']['complemento'] = $this->input->post('complemento');
        $dados['endereco']['cidade'] = $this->input->post('cidade');
        $dados['endereco']['estado'] = $this->input->post('estado');
        $dados['endereco']['cep'] = $this->input->post('cep');
        $dados['endereco']['bairro'] = $this->input->post('bairro');

        $dados['projeto']['id_endereco'] = $this->endereco->inserir_endereco($dados['endereco']);

        $dados['usuario_projeto']['id_projeto'] = $this->projeto->inserir_projeto($dados['projeto']);
        $dados['usuario_projeto']['id_usuario'] = $id;

        $cronograma['id_projeto'] = $dados['usuario_projeto']['id_projeto'];

        $dateTime = DateTime::createFromFormat('d/m/Y', $this->input->post('data_inicio'), new DateTimeZone('America/Sao_Paulo'));
        $timestamp = $dateTime->getTimestamp();
        $cronograma['data_inicio'] = date('Y-m-d h:m:s', $timestamp);
        
        $this->cronograma->inserir_cronograma($cronograma);

        $this->projeto->adicionar_usuario($dados['usuario_projeto']);

        for ($i=1; $i <= 11 ; $i++) { 
            if($this->input->post('usuario_'.$i) != "") {
                $dados['usuario_projeto']['id_usuario'] = $this->input->post('usuario_'.$i);
                $this->projeto->adicionar_usuario($dados['usuario_projeto']);
            }
        }

        redirect(base_url('area-adm/'.$_SESSION['nivel_usuario'].'/'.$dados['projeto']['status'].'/'.$id));
    }

    public function editar($id) {
        if(!$this->session->userdata('logged_in') || ( !($this->session->userdata('nivel_usuario') == "administrador")
            && !($this->session->userdata('nivel_usuario') == "funcionario")))
            redirect(base_url());

        $projetos['projetos_andamento'] = 0;
        $projetos['projetos_parados'] = 0;
        $projetos['projetos_concluidos'] = 0;

        $projetos['projeto_usuarios'] = $this->usuario->get_usuarios_projeto($id);
        $projetos['usuarios'] = $this->usuario->get_todos_usuarios();

        $this->form_validation->set_rules('descricao_projeto', 'Descrição', 'required');

        $this->form_validation->set_error_delimiters('<div class="container-error row">', '</div>');
        $this->form_validation->set_message('required','O campo "%s" é obrigatório');       

        $projetos['projeto'] = $this->projeto->get_projeto($id);
        $cronograma_temp = $this->cronograma->get_cronograma($id);
        $projetos['projeto']['data_inicio'] = date("d/m/Y", strtotime($cronograma_temp['data_inicio']));

        if ($this->form_validation->run()) {
            $this->editar_projeto($projetos['projeto']);
        } else {

            $post = $this->input->post();
            if($post == array()) {
                $_POST = $projetos['projeto'];    
            }

            $this->load->view('template/header');
            $this->load->view('template/menu');
            $this->load->view('area_adm/'.$_SESSION['nivel_usuario'].'/menu_projetos', $projetos);
            $this->load->view('formularios/editar_projeto');
            $this->load->view('template/footer');
        }
    }

    public function editar_projeto($projeto) {
        $dados['projeto']['id_projeto'] = $projeto['id_projeto'];
        $dados['projeto']['descricao_projeto'] = $this->input->post('descricao_projeto');
        $dados['projeto']['status'] = $this->input->post('status');
        $dados['projeto']['id_endereco'] = $projeto['id_endereco'];

        $dados['endereco']['id_endereco'] = $projeto['id_endereco'];
        $dados['endereco']['logradouro'] = $this->input->post('logradouro');
        $dados['endereco']['numero'] = $this->input->post('numero');
        $dados['endereco']['complemento'] = $this->input->post('complemento');
        $dados['endereco']['cidade'] = $this->input->post('cidade');
        $dados['endereco']['estado'] = $this->input->post('estado');
        $dados['endereco']['cep'] = $this->input->post('cep');
        $dados['endereco']['bairro'] = $this->input->post('bairro');

        $dados['cronograma']['id_projeto'] = $projeto['id_projeto'];
        $dateTime = DateTime::createFromFormat('d/m/Y', $this->input->post('data_inicio'), new DateTimeZone('America/Sao_Paulo'));
        $timestamp = $dateTime->getTimestamp();
        $dados['cronograma']['data_inicio'] = date('Y-m-d h:m:s', $timestamp);

        $this->endereco->editar_endereco($dados['endereco']);
        $this->projeto->editar_projeto($dados['projeto']);
        $this->cronograma->editar_cronograma($dados['cronograma']);

        redirect(base_url('area-adm/'.$_SESSION['nivel_usuario'].'/arquivos/'.$projeto['id_projeto']));
    }

    public function verificar_nome() {
        $query = $this->projeto->get_projeto_por_nome(ucwords(mb_strtolower($this->input->post('nome_projeto'), 'UTF-8')));

        if(sizeof($query)!=0) {
            $this->form_validation->set_message('verificar_nome', 'Nome de projeto já existe.');
            return false;
        } else {
            return true;
        }
    }

    public function pagamento($status, $id) {
        if(!$this->session->userdata('logged_in') || !($this->session->userdata('nivel_usuario') == "administrador"))
            redirect(base_url());

        $dados['id_projeto'] = $id;
        $dados['pago'] = $status;

        $this->projeto->editar_projeto($dados);

        redirect(base_url('/area-adm/'.$_SESSION['nivel_usuario'].'/pagamento/'.$id));
    }
}

?>