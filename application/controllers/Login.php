<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');

        date_default_timezone_set('America/Fortaleza');
    }

    public function index() {
        $this->form_validation->set_rules('usuario_login', 'Usuário', 'required');
        $this->form_validation->set_rules('senha_login', 'Senha', 'required|callback_validar_login');
        $this->form_validation->set_error_delimiters('<div class="container-error row">', '</div>');
        $this->form_validation->set_message('required','O campo "%s" é obrigatório');        

        if ($this->form_validation->run()) {
            $usuario = str_replace('.', '', $this->input->post('usuario_login'));
            $usuario = str_replace('-', '', $usuario);
            $usuario = str_replace('/', '', $usuario);
            $usuario = str_replace('\\', '', $usuario);
            $senha = md5($this->input->post('senha_login'));

            $query = $this->usuario->validate($usuario, $senha);
            
            $this->inicio_area_adm($query);
        } else {
            $this->load->view('template/header');
            $this->load->view('template/menu');
            $this->load->view('home');
            $this->load->view('template/footer');
        }
    }

    function validar_login() {
        $caracteres = array(".", "-", "/", "\\");
        $usuario = str_replace($caracteres, '', $this->input->post('usuario_login'));
        $senha = md5($this->input->post('senha_login'));

        $query = $this->usuario->validate($usuario, $senha);

        //debug_array($query);

        if((sizeof($query) == 1)) {
            return true;
        } else {
            $this->form_validation->set_message('validar_login', 'Usuário ou Senha inválidos');
            return false;
        }
    }

    public function inicio_area_adm($query) {
        
        $sessao = array(
                   'usuario'  => $query[0]['nome'],
                   'id_usuario'  => $query[0]['id_usuario'],
                   'email'     => $query[0]['email'],
                   'nivel_usuario'     => $query[0]['tipo'],
                   'logged_in' => TRUE
               );

            $this->session->set_userdata($sessao);

            $novo_acesso['id_usuario'] = $query[0]['id_usuario'];
            $this->acesso->adicionar_acesso($novo_acesso);

        redirect(base_url('area-adm'));
    }

    public function logout() {
        $sessao = array(
                   'usuario'  => "",
                   'email'     => "",
                   'nivel_usuario'     => "",
                   'logged_in' => FALSE
               );

        $this->session->set_userdata($sessao);

        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('home');
        $this->load->view('template/footer');
    }
}

?>