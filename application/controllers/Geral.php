<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Geral extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');

        date_default_timezone_set('America/Fortaleza');
    }

    public function index() {

        $this->form_validation->set_rules('usuario_login', 'Usuário', 'required');
        $this->form_validation->set_rules('senha_login', 'Senha', 'required');
        $this->form_validation->set_message('required','O campo "%s" é obrigatório');

        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('home');
        $this->load->view('template/footer');
    } 

    public function servicos($servico="") {
        $servico_item['servico'] = $servico;

        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('servicos', $servico_item);
        $this->load->view('template/footer');
    }

    public function servicos_iframe($servico="") {
        $servico_item['servico'] = $servico;

        $this->load->view('servicos_iframe', $servico_item);
    }

    public function portifolio() {
        $dados_projetos[1]['numero'] = 1;
        $dados_projetos[1]['titulo'] = "Projeto 1";
        $dados_projetos[1]['descricao'] = "Sed ut perspiciatis unde omnis iste natus error sit 
                                           voluptatem accusantium doloremque laudantium, totam 
                                           rem aperiam, eaque ipsa quae ab illo inventore veritatis 
                                           et quasi architecto beatae vitae dicta sunt explicabo. 1";

        $dados_projetos[2]['numero'] = 2;
        $dados_projetos[2]['titulo'] = "Projeto 2";
        $dados_projetos[2]['descricao'] = "Sed ut perspiciatis unde omnis iste natus error sit 
                                           voluptatem accusantium doloremque laudantium, totam 
                                           rem aperiam, eaque ipsa quae ab illo inventore veritatis 
                                           et quasi architecto beatae vitae dicta sunt explicabo. 2";

        $dados_projetos[3]['numero'] = 3;
        $dados_projetos[3]['titulo'] = "Projeto 3";
        $dados_projetos[3]['descricao'] = "Sed ut perspiciatis unde omnis iste natus error sit 
                                           voluptatem accusantium doloremque laudantium, totam 
                                           rem aperiam, eaque ipsa quae ab illo inventore veritatis 
                                           et quasi architecto beatae vitae dicta sunt explicabo. 3";

        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('portifolio');
        $this->load->view('projeto_portifolio', $dados_projetos[1]);
        $this->load->view('projeto_portifolio', $dados_projetos[2]);
        $this->load->view('projeto_portifolio', $dados_projetos[3]);
        $this->load->view('template/footer');
    }

    public function parceiros() {
        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('parceiros');
        $this->load->view('template/footer');
    }

    public function contato() {
        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('contato');
        $this->load->view('template/footer');
    }
}

?>