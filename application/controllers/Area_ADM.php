<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class Area_ADM extends CI_Controller {

    public function __construct() {
        parent::__construct();

        date_default_timezone_set('America/Fortaleza');
    }

    public function index() {

        if(!$this->session->userdata('logged_in')) redirect(base_url());

        switch ($this->session->userdata('nivel_usuario')) {
            case "administrador":
                $this->area_adm_administrador();
                break;

            case "funcionario":
                $this->area_adm_funcionario();
                break;

            case "cliente":
                $this->area_adm_cliente();
                break;

            case "cartorio":
                $this->area_adm_cartorio();
                break;

            case "prefeitura":
                $this->area_adm_prefeitura();
                break;

            default:
                redirect(base_url());
        }
    }

    private function area_adm_administrador() {
        redirect(base_url('area-adm/administrador/cliente'));
    }

    private function area_adm_funcionario() {
        redirect(base_url('area-adm/funcionario/cliente'));
    }

    private function area_adm_cliente() {
        $query = $this->acesso->get_acessos($_SESSION['id_usuario']);

        if(sizeof($query) == 1) {
            redirect(base_url('area-adm/cliente/criar-senha/'.$_SESSION['id_usuario']));
        } else {
            redirect(base_url('area-adm/cliente/andamento/'.$_SESSION['id_usuario']));
        }
    }

    private function area_adm_cartorio() {
        $query = $this->acesso->get_acessos($_SESSION['id_usuario']);

        if(sizeof($query) == 1) {
            redirect(base_url('area-adm/cartorio/criar-senha/'.$_SESSION['id_usuario']));
        } else {
            redirect(base_url('area-adm/cartorio/andamento/'.$_SESSION['id_usuario']));
        }
    }

    private function area_adm_prefeitura() {
        $query = $this->acesso->get_acessos($_SESSION['id_usuario']);

        if(sizeof($query) == 1) {
            redirect(base_url('area-adm/prefeitura/criar-senha/'.$_SESSION['id_usuario']));
        } else {
            redirect(base_url('area-adm/prefeitura/andamento/'.$_SESSION['id_usuario']));
        }
    }
}

?>