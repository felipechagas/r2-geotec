<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
/* 
| --------------------------------------------------------------
|	CONTROLLER DEFAULT
| --------------------------------------------------------------
*/
$route['default_controller'] = 'Geral';
$route['servicos/(:any)'] = 'Geral/servicos/$1';
$route['servicos_iframe/(:any)'] = 'Geral/servicos_iframe/$1';
$route['portifolio'] = 'Geral/portifolio';
$route['parceiros'] = 'Geral/parceiros';
$route['contato'] = 'Geral/contato';

/* 
| --------------------------------------------------------------
|	CONTROLLER AREA ADM
| --------------------------------------------------------------
*/
$route['area-adm'] = 'Area_ADM';

/* 
| --------------------------------------------------------------
|	CONTROLLER LOGIN
| --------------------------------------------------------------
*/
$route['login'] = 'Login';
$route['logout'] = 'Login/logout';

/* 
| --------------------------------------------------------------
|	CONTROLLER PREFEITURA
| --------------------------------------------------------------
*/
$route['area-adm/prefeitura/andamento/(:any)'] = 'Prefeitura/listar_projetos/andamento/$1';
$route['area-adm/prefeitura/parado/(:any)'] = 'Prefeitura/listar_projetos/parado/$1';
$route['area-adm/prefeitura/concluido/(:any)'] = 'Prefeitura/listar_projetos/concluido/$1';

$route['area-adm/prefeitura/arquivos/(:any)'] = 'Prefeitura/listar_arquivos/$1';
$route['area-adm/prefeitura/historico/(:any)'] = 'Prefeitura/listar_historico/$1';

$route['area-adm/prefeitura/criar-senha/(:any)'] = 'Prefeitura/criar_senha/$1';

/* 
| --------------------------------------------------------------
|	CONTROLLER CARTORIO
| --------------------------------------------------------------
*/
$route['area-adm/cartorio/andamento/(:any)'] = 'Cartorio/listar_projetos/andamento/$1';
$route['area-adm/cartorio/parado/(:any)'] = 'Cartorio/listar_projetos/parado/$1';
$route['area-adm/cartorio/concluido/(:any)'] = 'Cartorio/listar_projetos/concluido/$1';

$route['area-adm/cartorio/arquivos/(:any)'] = 'Cartorio/listar_arquivos/$1';
$route['area-adm/cartorio/historico/(:any)'] = 'Cartorio/listar_historico/$1';

$route['area-adm/cartorio/criar-senha/(:any)'] = 'Cartorio/criar_senha/$1';

/* 
| --------------------------------------------------------------
|	CONTROLLER CLIENTE
| --------------------------------------------------------------
*/
$route['area-adm/cliente/andamento/(:any)'] = 'Cliente/listar_projetos/andamento/$1';
$route['area-adm/cliente/parado/(:any)'] = 'Cliente/listar_projetos/parado/$1';
$route['area-adm/cliente/concluido/(:any)'] = 'Cliente/listar_projetos/concluido/$1';

$route['area-adm/cliente/arquivos/(:any)'] = 'Cliente/listar_arquivos/$1';
$route['area-adm/cliente/historico/(:any)'] = 'Cliente/listar_historico/$1';

$route['area-adm/cliente/criar-senha/(:any)'] = 'Cliente/criar_senha/$1';

/* 
| --------------------------------------------------------------
|	CONTROLLER FUNCIONARIO
| --------------------------------------------------------------
*/
$route['area-adm/funcionario/cliente'] = 'Funcionario/listar_usuarios/cliente';
$route['area-adm/funcionario/cartorio'] = 'Funcionario/listar_usuarios/cartorio';
$route['area-adm/funcionario/prefeitura'] = 'Funcionario/listar_usuarios/prefeitura';

$route['area-adm/funcionario/andamento/(:any)'] = 'Funcionario/listar_projetos/andamento/$1';
$route['area-adm/funcionario/parado/(:any)'] = 'Funcionario/listar_projetos/parado/$1';
$route['area-adm/funcionario/concluido/(:any)'] = 'Funcionario/listar_projetos/concluido/$1';

$route['area-adm/funcionario/arquivos/(:any)'] = 'Funcionario/listar_arquivos/$1';
$route['area-adm/funcionario/pagamento/(:any)'] = 'Funcionario/listar_pagamento/$1';
$route['area-adm/funcionario/historico/(:any)'] = 'Funcionario/listar_historico/$1';
$route['area-adm/funcionario/gastos/(:any)'] = 'Funcionario/listar_gastos/$1';
$route['area-adm/funcionario/cronograma/(:any)'] = 'Funcionario/cronograma/$1';

/* 
| --------------------------------------------------------------
|	CONTROLLER ADM
| --------------------------------------------------------------
*/
$route['area-adm/administrador/cliente'] = 'Administrador/listar_usuarios/cliente';
$route['area-adm/administrador/cartorio'] = 'Administrador/listar_usuarios/cartorio';
$route['area-adm/administrador/prefeitura'] = 'Administrador/listar_usuarios/prefeitura';

$route['area-adm/administrador/andamento/(:any)'] = 'Administrador/listar_projetos/andamento/$1';
$route['area-adm/administrador/parado/(:any)'] = 'Administrador/listar_projetos/parado/$1';
$route['area-adm/administrador/concluido/(:any)'] = 'Administrador/listar_projetos/concluido/$1';

$route['area-adm/administrador/arquivos/(:any)'] = 'Administrador/listar_arquivos/$1';
$route['area-adm/administrador/pagamento/(:any)'] = 'Administrador/listar_pagamento/$1';
$route['area-adm/administrador/historico/(:any)'] = 'Administrador/listar_historico/$1';
$route['area-adm/administrador/gastos/(:any)'] = 'Administrador/listar_gastos/$1';
$route['area-adm/administrador/cronograma/(:any)'] = 'Administrador/cronograma/$1';


/* 
| --------------------------------------------------------------
|	CONTROLLER PROJETO
| --------------------------------------------------------------
*/

$route['area-adm/adicionar-projeto/(:any)'] = 'Projeto_Controller/adicionar/$1';
$route['area-adm/pagamento/pago/(:any)'] = 'Projeto_Controller/pagamento/total/$1';
$route['area-adm/pagamento/em-dias/(:any)'] = 'Projeto_Controller/pagamento/parcial/$1';
$route['area-adm/pagamento/atrasado/(:any)'] = 'Projeto_Controller/pagamento/atrasado/$1';

$route['area-adm/editar/projeto/(:any)'] = 'Projeto_Controller/editar/$1';

/* 
| --------------------------------------------------------------
|	CONTROLLER USUARIO
| --------------------------------------------------------------
*/
$route['area-adm/adicionar/cliente'] = 'Usuario_Controller/adicionar/cliente';
$route['area-adm/adicionar/cartorio'] = 'Usuario_Controller/adicionar/cartorio';
$route['area-adm/adicionar/prefeitura'] = 'Usuario_Controller/adicionar/prefeitura';

$route['area-adm/editar/cliente/(:any)'] = 'Usuario_Controller/editar/$1';
$route['area-adm/editar/cartorio/(:any)'] = 'Usuario_Controller/editar/$1';
$route['area-adm/editar/prefeitura/(:any)'] = 'Usuario_Controller/editar/$1';

$route['area-adm/deletar/projeto/(:any)/(:any)'] = 'Usuario_Controller/deletar_usuario_projeto/$1/$2';
$route['area-adm/adicionar/usuario-projeto/(:any)/(:any)'] = 'Usuario_Controller/adicionar_usuario_projeto/$1/$2';
$route['area-adm/visibilidade/(:any)/(:any)/(:any)'] = 'Usuario_Controller/visibilidade/$1/$2/$3';

/* 
| --------------------------------------------------------------
|	CONTROLLER CRONOGRAMA
| --------------------------------------------------------------
*/
$route['area-adm/cronograma/previsao/(:any)'] = 'Cronograma_Controller/editar/$1';
$route['area-adm/concluir-projeto/(:any)'] = 'Cronograma_Controller/concluir/$1';

/* 
| --------------------------------------------------------------
|	CONTROLLER RELATORIO
| --------------------------------------------------------------
*/
$route['area-adm/adicionar-historico/(:any)'] = 'Relatorio_Controller/adicionar/$1';
$route['historico/deletar/(:any)'] = 'Relatorio_Controller/deletar/$1';
$route['area-adm/historico/editar/(:any)/(:any)'] = 'Relatorio_Controller/editar/$1/$2';

/* 
| --------------------------------------------------------------
|	CONTROLLER GASTO
| --------------------------------------------------------------
*/
$route['area-adm/adicionar-gasto/(:any)'] = 'Gasto_Controller/adicionar/$1';
$route['gasto/deletar/(:any)'] = 'Gasto_Controller/deletar/$1';

/* 
| --------------------------------------------------------------
|	CONTROLLER PAGAMENTO
| --------------------------------------------------------------
*/
$route['area-adm/adicionar-pagamentos/(:any)'] = 'Pagamento_Controller/adicionar/$1';
$route['pagamento/deletar/(:any)'] = 'Pagamento_Controller/deletar/$1';
$route['area-adm/pagamento/editar/(:any)'] = 'Pagamento_Controller/editar/$1';

/* 
| --------------------------------------------------------------
|	CONTROLLER ARQUIVO
| --------------------------------------------------------------
*/
$route['area-adm/adicionar-arquivos/(:any)'] = 'Arquivo_Controller/adicionar/$1';
$route['download/(:any)'] = 'Arquivo_Controller/download/$1';
$route['download-todos/(:any)'] = 'Arquivo_Controller/download_todos/$1';
$route['arquivo/deletar/(:any)'] = 'Arquivo_Controller/deletar/$1';

/* 
| --------------------------------------------------------------
|	ERROR
| --------------------------------------------------------------
*/

$route['404_override'] = '';


$route['translate_uri_dashes'] = FALSE;
