		<div class="row">
			<?php if(sizeof($arquivos_por_formato) != 0) { ?>
				<p>
				<div class="row">
					<div class="col-md-12 col-lg-12 text-left">
						<a href="<?php echo base_url("download-todos/".$projeto['id_projeto']); ?>" class="btn btn-success">
							Baixar Todos <img src="<?php echo base_url('publico/imagens/download.png'); ?>"/>
						</a>
					</div>
				</div>
			<?php } ?>		
			<p>
			<?php foreach ($arquivos_por_formato as $arquivos) { ?>
				<div class="row">
					<h4 class="text-left">
						<?php
							foreach ($arquivos as $arquivo) {
								echo ucwords(mb_strtolower($arquivo['formato'], 'UTF-8'));
								break;
							}
						?>
					</h4>
				</div>
				<?php foreach ($arquivos as $arquivo) { ?>
					<?php if($arquivo['tipo'] == 'confidencial') continue; ?>

					<div class="row destaque-linha">
						<div class="col-md-6 col-lg-6 text-left">
							<a href="<?php echo base_url("download/".$arquivo['id_arquivo']); ?>" class="btn btn-default btn-lista btn-lista-arquivos">
								<span><?php echo $arquivo['nome'].".".$arquivo['formato']; ?></span>
							</a>
						</div>
						<div class="col-md-1 col-lg-1 text-center">
							<div class="col-md-4 col-lg-4 text-center">
								<a href="<?php echo base_url("download/".$arquivo['id_arquivo']); ?>" class="btn btn-default btn-formato-arquivo">
									<img src="<?php echo imagem_arquivo($arquivo['formato']); ?>"/>
								</a>
							</div>
						</div>
						<div class="col-md-5 col-lg-5 text-right">
							<a href="<?php echo base_url("download/".$arquivo['id_arquivo']); ?>" class="btn btn-success">
								Baixar <img src="<?php echo base_url('publico/imagens/download.png'); ?>"/>
							</a>

							<a href="<?php echo base_url("arquivo/deletar/".$arquivo['id_arquivo']); ?>" 
								onclick="return confirm('Deseja remover esse registro?');"class="btn btn-danger">
								Deletar <img src="<?php echo base_url('publico/imagens/deletar.png'); ?>"/>
							</a>
						</div>
					</div>
				<?php } ?>
			<?php } ?>
			<?php if(sizeof($arquivos_por_formato) != 0) { ?>
				<p>
				<div class="row">
					<div class="col-md-12 col-lg-12 text-left">
						<a href="<?php echo base_url("download-todos/".$projeto['id_projeto']); ?>" class="btn btn-success">
							Baixar Todos <img src="<?php echo base_url('publico/imagens/download.png'); ?>"/>
						</a>
					</div>
				</div>
			<?php } ?>	
		</div>
	</div>
</div>