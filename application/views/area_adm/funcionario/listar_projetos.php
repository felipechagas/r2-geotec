	<div class="col-md-8 col-lg-8">
		<?php if($usuario['tipo'] == "cliente") { ?>
			<div class="row">
				<div class="col-md-5 col-lg-5 text-left">
					<a href="<?php echo base_url('area-adm/adicionar-projeto/'.$usuario['id_usuario']); ?>"  class="botao-login btn btn-info btn-sm">Adicionar Projeto</a>
				</div>
			</div>
		<?php } ?>
		<p>
		<div class="row">
			
			<?php foreach ($projetos as $projeto) { 
				if(!isset($projeto['nome_projeto'])) continue;?>
				<div class="row destaque-linha">
					<p>
					<div class="col-md-3 col-lg-3 text-left">
						<a href="<?php echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/arquivos/'.$projeto['id_projeto']); ?>" class="hidden-md btn btn-default btn-lista">
							<span><?php echo substr($projeto['nome_projeto'], 0, 23); ?></span>
						</a>
						<a href="<?php echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/arquivos/'.$projeto['id_projeto']); ?>" class="hidden-lg btn btn-default btn-lista">
							<span><?php echo substr($projeto['nome_projeto'], 0, 15); ?>...</span>
						</a>
					</div>

					<div class="col-md-8 col-lg-8 text-left btn-lista-contagem-caixa">
						<a href="<?php echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/arquivos/'.$projeto['id_projeto']); ?>" class="hidden-md btn btn-default btn-lista">
							<span><?php echo substr($projeto['descricao_projeto'], 0, 70); ?>...</span>
						</a>
						<a href="<?php echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/arquivos/'.$projeto['id_projeto']); ?>" class="hidden-lg btn btn-default btn-lista">
							<span><?php echo substr($projeto['descricao_projeto'], 0, 50); ?>...</span>
						</a>
					</div>
				</div>
			<?php }?>
		</div>
	</div>
</div>