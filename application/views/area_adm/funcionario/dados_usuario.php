<div class="row">
	<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
		<div class="row">
			<div class="col-md-9 col-lg-9 dados-clientes">
				<div class="row fundo-azul">
					<h4>
						Dados do Usuário
					</h4>
				</div>
				<div class="row dados-clientes-padding">
					<div class="row dados-clientes-linha">
						<div class="col-md-12 col-lg-12 text-left">
							<span><b>Nome:</b> <?php echo $usuario['nome'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col-md-5 col-lg-5 text-left">
							<span><b>CNPJ:</b> <?php echo $usuario['cnpj'].'.'; ?></span>
						</div>
						<div class="col-md-6 col-lg-6 text-left">
							<span><b>CPF:</b> <?php echo $usuario['cpf'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col-md-5 col-lg-5 text-left">
							<span><b>E-mail:</b> <?php echo $usuario['email'].'.'; ?></span>
						</div>
						<div class="col-md-3 col-lg-3 text-left">
							<span><b>Fixo:</b> <?php echo $usuario['telefone_1'].'.'; ?></span>
						</div>
						<div class="col-md-4 col-lg-4 text-right">
							<span><b>Cel.:</b> <?php echo $usuario['telefone_2'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col-md-5 col-lg-5 text-left">
							<span><b>Cidade:</b> <?php echo $usuario['cidade'].' - '.$usuario['estado'].'.'; ?></span>
						</div>
						<div class="col-md-3 col-lg-3 text-left">
							<span><b>CEP:</b> <?php echo $usuario['cep'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col-md-8 col-lg-8 text-left">
							<span><b>Endereço:</b> <?php echo $usuario['logradouro'].', '.$usuario['numero'].'. '.$usuario['bairro'].'.'; ?></span>
						</div>
						<div class="col-md-4 col-lg-4 text-left">
							<span><b>Comp.:</b> <?php echo $usuario['complemento'].'.'; ?></span>
						</div>
					</div>
					<p>
					<div class="col-md-12 col-lg-12 text-right">
						<a href="<?php echo base_url('area-adm/editar/'.$usuario['tipo'].'/'.$usuario['id_usuario']); ?>"  class="botao-login btn btn-warning">
							Editar Dados do Usuário
							<img src="<?php echo base_url('publico/imagens/editar.png'); ?>"/>
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-lg-3 dados-clientes">
				<div class="row fundo-azul">
					<h4>
						Histórico de Acessos
					</h4>
				</div>
				<div class="row lista-acessos">
					<?php foreach ($acessos as $acesso) { ?>
						<div class="row dados-clientes-linha">
							<div class="col-md-2 col-lg-2 text-center">
								<span>Dia</span>
							</div>
							<div class="col-md-5 col-lg-5 text-center">
								<span><b><?php echo date("d/m/Y", strtotime($acesso['data_hora'])); ?></b></span>
							</div>
							<div class="col-md-1 col-lg-1 text-center">
								<span>às</span>
							</div>
							<div class="col-md-3 col-lg-3 text-center">
								<span><b><?php echo date("h:m", strtotime($acesso['data_hora'])); ?></b></span>
							</div>
							<div class="col-md-1 col-lg-1 text-center">
								<span>h.</span>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>