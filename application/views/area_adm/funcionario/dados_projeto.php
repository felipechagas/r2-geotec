<div class="row">
	<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
		<div class="row">
			<div class="col-md-8 col-lg-8 dados-projeto">
				<div class="row fundo-azul">
					<h4>
						Dados do Projeto
					</h4>
				</div>
				<div class="row dados-clientes-padding">
					<div class="row dados-clientes-linha">
						<div class="col-md-12 col-lg-12 text-left">
							<span><b>Nome:</b> <?php echo $projeto['nome_projeto'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col-md-12 col-lg-12 text-left">
							<span><b>Descrição:</b> <?php echo $projeto['descricao_projeto'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col-md-12 col-lg-12 text-left">
							<span><b>Status:</b> <?php echo $projeto['status'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col-md-5 col-lg-5 text-left">
							<span><b>Cidade:</b> <?php echo $projeto['cidade'].' - '.$projeto['estado'].'.'; ?></span>
						</div>
						<div class="col-md-3 col-lg-3 text-left">
							<span><b>CEP:</b> <?php echo $projeto['cep'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col--8 col-lg-8 text-left">
							<span><b>Endereço:</b> <?php echo $projeto['logradouro'].', '.$projeto['numero'].'. '.$projeto['bairro'].'.'; ?></span>
						</div>
						<div class="col-md-4 col-lg-4 text-left">
							<span><b>Comp.:</b> <?php echo $projeto['complemento'].'.'; ?></span>
						</div>
					</div>
					<p>
					<div class="col-md-12 col-lg-12 text-right">
						<a href="<?php echo base_url('area-adm/editar/projeto/'.$projeto['id_projeto']); ?>"  class="botao-login btn btn-warning">
							Editar Dados do Projeto
							<img src="<?php echo base_url('publico/imagens/editar.png'); ?>"/>
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-lg-4 dados-projeto">
				<div class="row fundo-azul">
					<h4>
						Usúarios do Projeto
					</h4>
				</div>

				<!-- Clientes do Projeto-->
				<div class="row lista-usuarios">
					<div class="row dados-clientes-linha">
						<div class="col-md-12 col-lg-12 text-left">
							<h4>Clientes: </h4>
						</div>
						<?php
							$count_clientes = 0;
							$count_prefeitura = 0;
							$count_cartorio = 0;
							foreach ($usuarios as $usuario) {
								if($usuario['tipo'] == 'cliente') $count_clientes++;
								if($usuario['tipo'] == 'prefeitura') $count_prefeitura++;
								if($usuario['tipo'] == 'cartorio') $count_cartorio++;
							}
						?>
						
						<?php foreach ($usuarios as $usuario) { 
							if($usuario['tipo'] == 'cliente') { ?>
								<div class="row dados-clientes-linha usuarios-do-projeto">
									<div class="col-md-12 col-lg-12 text-left">
										<span><a class="btn btn-info" href="<?php echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/andamento/'.$usuario['id_usuario']); ?>"><?php echo $usuario['nome']; ?></a></span>
										<?php if($count_clientes > 1) { ?>
											<a href="<?php echo base_url('area-adm/deletar/projeto/'.$projeto['id_projeto'].'/'.$usuario['id_usuario']); ?>" 
												onclick="return confirm('Deseja remover o usuario desse projeto?');"class="btn btn-danger">
												<img src="<?php echo base_url('publico/imagens/deletar.png'); ?>"/>
											</a>
										<?php } ?>
									</div>
								</div>
							<?php } ?>
						<?php } ?>

						<div class="row dados-clientes-linha usuarios-do-projeto">
							<form method="POST" action="<?php echo base_url('area-adm/adicionar/usuario-projeto/'.$projeto['id_projeto'].'/cliente'); ?>">
								<div class="form-group row">
						            <div class="col-md-10 col-lg-10">
						            	<select name="usuario_cliente" class="chosen-select form-control" >
							          		<option value="" <?php echo  set_select('usuario_cliente', '', TRUE); ?>>-Adicionar cliente ao projeto-</option>-->
	
											<?php foreach ($clientes as $cliente) { ?>
												<option value="<?php echo $cliente['id_usuario'] ?>" <?php echo  set_select('usuario_cliente', $cliente['id_usuario']); ?> ><?php echo $cliente['nome'] ?></option>
											<?php } ?>
								    	</select>
						            </div>
						            <div class="col-md-2 col-lg-2">
					            		<button type="submit" class="btn btn-success">
											<img src="<?php echo base_url('publico/imagens/incluir.png'); ?>"/>
										</button>
									</div>
						      </div>
							</form>
						</div>

						<!-- Prefeituras do Projeto-->
						<?php foreach ($usuarios as $usuario) {
							if($usuario['tipo'] == 'prefeitura') { ?>
								<div class="col-md-12 col-lg-12 text-left">
									<h4>Prefeitura: </h4>
								</div>
								<div class="row dados-clientes-linha usuarios-do-projeto">
									<div class="col-md-12 col-lg-12 text-left">
										<span><a class="btn btn-info" href="<?php echo base_url('area-adm/adicionar/usuario-projeto/'.$projeto['id_projeto'].'/prefeitura'); ?>"><?php echo $usuario['nome']; ?></a></span>
										<a href="<?php echo base_url('area-adm/deletar/projeto/'.$projeto['id_projeto'].'/'.$usuario['id_usuario']); ?>" 
											onclick="return confirm('Deseja remover o usuario desse projeto?');"class="btn btn-danger">
											<img src="<?php echo base_url('publico/imagens/deletar.png'); ?>"/>
										</a>
									</div>
								</div>
							<?php } ?>
						<?php } ?>

						<?php if($count_prefeitura == 0) { ?>
							<div class="row dados-clientes-linha usuarios-do-projeto">
								<form method="POST" action="<?php echo base_url('area-adm/adicionar/usuario-projeto/'.$projeto['id_projeto'].'/prefeitura'); ?>">
									<div class="form-group row">
							            <div class="col-md-10 col-lg-10">
							            	<select name="usuario_prefeitura" class="chosen-select form-control" >
								          		<option value="" <?php echo  set_select('usuario_prefeitura', '', TRUE); ?>>-Adicionar Prefeitura-</option>
		
												<?php foreach ($prefeituras as $prefeitura) { ?>
													<option value="<?php echo $prefeitura['id_usuario'] ?>" <?php echo  set_select('usuario_prefeitura', $prefeitura['id_usuario']); ?> ><?php echo $prefeitura['nome'] ?></option>
												<?php } ?>
									    	</select>
							            </div>
							            <div class="col-md-2 col-lg-2">
						            		<button type="submit" class="btn btn-success">
												<img src="<?php echo base_url('publico/imagens/incluir.png'); ?>"/>
											</button>
										</div>
							        </div>
								</form>
							</div>
						<?php } ?>
						<!-- Cartorios do Projeto-->
						<?php foreach ($usuarios as $usuario) {
							if($usuario['tipo'] == 'cartorio') { ?>
								<div class="col-md-12 col-lg-12 text-left">
									<h4>Cartório: </h4>
								</div>
								<div class="row dados-clientes-linha usuarios-do-projeto">
									<div class="col-md-12 col-lg-12 text-left">
										<span><a class="btn btn-info" href="<?php echo base_url('area-adm/adicionar/usuario-projeto/'.$projeto['id_projeto'].'/cartorio'); ?>"><?php echo $usuario['nome']; ?></a></span>
										<a href="<?php echo base_url('area-adm/deletar/projeto/'.$projeto['id_projeto'].'/'.$usuario['id_usuario']); ?>" 
											onclick="return confirm('Deseja remover o usuario desse projeto?');"class="btn btn-danger">
											<img src="<?php echo base_url('publico/imagens/deletar.png'); ?>"/>
										</a>
									</div>
								</div>
							<?php } ?>
						<?php } ?>

						<?php if($count_cartorio == 0) { ?>
							<div class="row dados-clientes-linha usuarios-do-projeto">
								<form method="POST" action="<?php echo base_url('area-adm/adicionar/usuario-projeto/'.$projeto['id_projeto'].'/cartorio'); ?>">
									<div class="form-group row">
							            <div class="col-md-10 col-lg-10">
							            	<select name="usuario_cartorio" class="chosen-select form-control" >
								          		<option value="" <?php echo  set_select('usuario_cartorio', '', TRUE); ?>>-Adicionar Cartorio-</option>
		
												<?php foreach ($cartorios as $cartorio) { ?>
													<option value="<?php echo $cartorio['id_usuario'] ?>" <?php echo  set_select('usuario_cartorio', $cartorio['id_usuario']); ?> ><?php echo $cartorio['nome'] ?></option>
												<?php } ?>
									    	</select>
							            </div>
							            <div class="col-md-2 col-lg-2">
						            		<button type="submit" class="btn btn-success">
												<img src="<?php echo base_url('publico/imagens/incluir.png'); ?>"/>
											</button>
										</div>
							      </div>
								</form>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>