<?php 
  $URI = preg_replace('/\/$|\/r2geotec\//i', '', $_SERVER['REQUEST_URI']);
  $clientes = strpos($URI, 'cliente') !== FALSE;
  $cartorios = strpos($URI, 'cartorio') !== FALSE;
  $prefeituras = strpos($URI, 'prefeitura') !== FALSE;
?>

<div class="row">
	
</div>

<div class="row">
	
	<nav class="hidden-xs hidden-sm col-md-2 col-lg-2 col-md-offset-1 col-lg-offset-1">
		<ul class="nav nav-pills nav-stacked menu-lateral">
		  <li class="<?php echo ($clientes ? ' active' : '') ?>">
		    <a href="<?php echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/cliente'); ?>"><b>Clientes</b></a>
		  </li>

		  <li class="<?php echo ($cartorios ? ' active' : '') ?>">
		    <a href="<?php echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/cartorio'); ?>"><b>Cartórios</b></a>
		  </li>
		  
		  <li class="<?php echo ($prefeituras ? ' active' : '') ?>">
		    <a href="<?php echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/prefeitura'); ?>"><b>Prefeituras</b></a>
		  </li>
		</ul>
	</nav>