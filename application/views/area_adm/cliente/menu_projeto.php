<?php 
  $URI = preg_replace('/\/$|\/r2geotec\//i', '', $_SERVER['REQUEST_URI']);
  $arquivos = strpos($URI, 'arquivo') !== FALSE;
  $pagamento = strpos($URI, 'pagamento') !== FALSE;
  $historico = strpos($URI, 'historico') !== FALSE;
  $gastos = strpos($URI, 'gasto') !== FALSE;
  $cronograma = strpos($URI, 'cronograma') !== FALSE;
?>

<div class="row">
	
	<nav class="hidden-xs hidden-sm col-md-2 col-lg-2 col-md-offset-1 col-lg-offset-1">
		<ul class="nav nav-pills nav-stacked menu-lateral">
		  <li class="<?php echo ($arquivos ? ' active' : '') ?>">
		    <a href="<?php echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/arquivos/'.$projeto['id_projeto']); ?>"><b>Arquivos</b></a>
		  </li>

		  <li class="<?php echo ($historico ? ' active' : '') ?>">
		    <a href="<?php echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/historico/'.$projeto['id_projeto']); ?>"><b>Historico</b></a>
		  </li>
		</ul>
	</nav>