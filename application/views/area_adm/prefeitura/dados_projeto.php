<div class="row">
	<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
		<div class="row">
			<div class="col-md-8 col-lg-8 dados-projeto">
				<div class="row fundo-azul">
					<h4>
						Dados do Projeto
					</h4>
				</div>
				<div class="row dados-clientes-padding">
					<div class="row dados-clientes-linha">
						<div class="col-md-12 col-lg-12 text-left">
							<span><b>Nome:</b> <?php echo $projeto['nome_projeto'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col-md-12 col-lg-12 text-left">
							<span><b>Descrição:</b> <?php echo $projeto['descricao_projeto'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col-md-12 col-lg-12 text-left">
							<span><b>Status:</b> <?php echo $projeto['status'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col-md-5 col-lg-5 text-left">
							<span><b>Cidade:</b> <?php echo $projeto['cidade'].' - '.$projeto['estado'].'.'; ?></span>
						</div>
						<div class="col-md-3 col-lg-3 text-left">
							<span><b>CEP:</b> <?php echo $projeto['cep'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col--8 col-lg-8 text-left">
							<span><b>Endereço:</b> <?php echo $projeto['logradouro'].', '.$projeto['numero'].'. '.$projeto['bairro'].'.'; ?></span>
						</div>
						<div class="col-md-4 col-lg-4 text-left">
							<span><b>Comp.:</b> <?php echo $projeto['complemento'].'.'; ?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-lg-4 dados-projeto">
				<div class="row fundo-azul">
					<h4>
						Usúarios do Projeto
					</h4>
				</div>

				<!-- Clientes do Projeto-->
				<div class="row lista-usuarios">
					<div class="row dados-clientes-linha">
						<div class="col-md-12 col-lg-12 text-left">
							<h5><b>Usuários do Projeto: </b></h5>
						</div>

						<?php foreach ($usuarios as $usuario) { 
							if($usuario['tipo'] == 'cliente') { ?>
								<?php if($usuario['id_usuario'] == $_SESSION['id_usuario']) { ?>
									<div class="row dados-clientes-linha usuarios-do-projeto">
										<div class="col-md-12 col-lg-12 text-left">
											<span><a class="btn btn-info" href="<?php echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/andamento/'.$usuario['id_usuario']); ?>"><?php echo $usuario['nome']; ?></a></span>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						<?php } ?>
						
						<?php foreach ($usuarios as $usuario) { 
							if($usuario['tipo'] == 'cliente') { ?>
								<?php if($usuario['id_usuario'] != $_SESSION['id_usuario']) { ?>
									<div class="row dados-clientes-linha usuarios-do-projeto">
										<div class="col-md-12 col-lg-12 text-left">
											<span><?php echo $usuario['nome']; ?></span>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						<?php } ?>

						

						<!-- Prefeituras do Projeto-->
						<?php foreach ($usuarios as $usuario) {
							if($usuario['tipo'] == 'prefeitura') { ?>
								<div class="col-md-12 col-lg-12 text-left">
									<h5><b>Prefeitura: </b></h5>
								</div>
								<div class="row dados-clientes-linha usuarios-do-projeto">
									<div class="col-md-12 col-lg-12 text-left">
										<span><a class="btn btn-info" href="<?php echo base_url('area-adm'); ?>"><?php echo $usuario['nome']; ?></a></span>
									</div>
								</div>
							<?php } ?>
						<?php } ?>

						<!-- Cartorios do Projeto-->
						<?php foreach ($usuarios as $usuario) {
							if($usuario['tipo'] == 'cartorio') { ?>
								<div class="col-md-12 col-lg-12 text-left">
									<h5><b>Cartório: </b></h5>
								</div>
								<div class="row dados-clientes-linha usuarios-do-projeto">
									<div class="col-md-12 col-lg-12 text-left">
										<span><?php echo $usuario['nome']; ?></span>
									</div>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>