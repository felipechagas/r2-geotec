		<div class="row">
			<?php foreach ($relatorios as $relatorio) { ?>
				<p>
				<div class="row destaque-linha">
					<div class="row">
						<p>
						<div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1 text-left">
							<span><b>Dia </b><?php echo date("d/m/Y", strtotime($relatorio['data_relatorio']))."."; ?></span>
						</div>
						<p>
						<div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1  text-left">
							<span><b>Relatório: </b><?php echo $relatorio['descricao']."."; ?></span>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-lg-12 text-right">
							<?php if($relatorio['compartilhado'] == 0) { ?>
								<a onclick="return confirm('Tem certeza que deseja compartilhar este relatório com o cliente?');" href="<?php echo base_url('area-adm/historico/editar/'.$relatorio['id_relatorio'].'/1'); ?>" class="btn btn-warning">
									Compartilhar com Cliente <img src="<?php echo base_url('publico/imagens/share.png'); ?>"/>
								</a>
							<?php } else { ?>
								<a onclick="return confirm('Tem certeza que deseja impedir o cliente de ver esse relatório?');" href="<?php echo base_url('area-adm/historico/editar/'.$relatorio['id_relatorio'].'/0'); ?>" class="btn btn-warning">
									Privar do Cliente <img src="<?php echo base_url('publico/imagens/unshare.png'); ?>"/>
								</a>
							<?php } ?>

							<a href="<?php echo base_url('historico/deletar/'.$relatorio['id_relatorio']); ?>" 
								onclick="return confirm('Deseja remover esse registro?');"class="btn btn-danger">
								Deletar <img src="<?php echo base_url('publico/imagens/deletar.png'); ?>"/>
							</a>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>