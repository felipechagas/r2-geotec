	<div class="col-md-8 col-lg-8">
		<div class="row">
			<div class="col-md-12 col-lg-12 text-left">
				<a href="<?php echo base_url('area-adm/adicionar/'.$tipo); ?>"  class="botao-login btn btn-info btn-sm">Adicionar <?php echo $tipo; ?></a>
			</div>
		</div>
		<p>
		<div class="row">
			<?php foreach ($usuarios as $usuario) { ?>
				<div class="row destaque-linha">
					<p>
					<div class="col-md-5 col-lg-5 text-left">
						<a href="<?php echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/andamento/'.$usuario['id_usuario']); ?>" class="hidden-md btn btn-default btn-lista">
							<span><?php echo substr($usuario['nome'], 0, 45); ?></span>
						</a>
						<a href="<?php echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/andamento/'.$usuario['id_usuario']); ?>" class="hidden-lg btn btn-default btn-lista">
							<span><?php echo substr($usuario['nome'], 0, 35); ?></span>
						</a>
					</div>
					<div class="col-md-2 col-lg-2 text-left btn-lista-contagem-caixa">
						<a href="<?php if( $usuario['numero_projetos_concluidos'] != 0 ) echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/concluido/'.$usuario['id_usuario']); ?>" class="btn btn-success btn-lista-contagem <?php if( $usuario['numero_projetos_concluidos'] == 0 ) echo 'link-bloqueado';?>">
							<span>Concluidos: <?php echo $usuario['numero_projetos_concluidos']; ?>.</span>
						</a>
					</div>
					<div class="col-md-2 col-lg-2 text-left btn-lista-contagem-caixa">
						<a href="<?php if( $usuario['numero_projetos_andamentos'] != 0 ) echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/andamento/'.$usuario['id_usuario']); ?>" class="btn btn-warning btn-lista-contagem <?php if( $usuario['numero_projetos_andamentos'] == 0 ) echo 'link-bloqueado';?>">
							<span>Em Andam.: <?php echo $usuario['numero_projetos_andamentos']; ?>.</span>
						</a>
					</div>
					<div class="col-md-2 col-lg-2 text-left btn-lista-contagem-caixa">
						<a href="<?php if( $usuario['numero_projetos_parados'] != 0 ) echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/parado/'.$usuario['id_usuario']); ?>" class="btn btn-danger btn-lista-contagem <?php if( $usuario['numero_projetos_parados'] == 0 ) echo 'link-bloqueado';?>">
							<span>Parados: <?php echo $usuario['numero_projetos_parados']; ?>.</span>
						</a>
					</div>				
				</div>
			<?php }?>
		</div>
	</div>
</div>