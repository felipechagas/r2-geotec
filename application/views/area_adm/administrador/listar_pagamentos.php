		<div class="row">
			<div class="row">
				<div class="col-md-2 col-lg-2 text-left">
					<b>Marcar Como:</b>
				</div>
				<div class="col-md-10 col-lg-10 text-left">

					<a href="<?php echo base_url('area-adm/pagamento/pago/'.$projeto['id_projeto']); ?>" class="btn btn-success">
						Projeto Pago
					</a>
					<a href="<?php echo base_url('area-adm/pagamento/em-dias/'.$projeto['id_projeto']); ?>" class="btn btn-warning">
						Projeto Em Dias
					</a>
					<a href="<?php echo base_url('area-adm/pagamento/atrasado/'.$projeto['id_projeto']); ?>" class="btn btn-danger">
						Pagamen. Atrasado
					</a>
				</div>
			</div>
			<?php foreach ($pagamentos as $pagamento) { ?>
				<p>
				<div class="row destaque-linha">
					<div class="row">
						<p>
						<div class="col-md-12 col-lg-12 text-left">
							<span><b>Pagamento: </b><?php echo "R$ ".$pagamento['valor_recebido']."."; ?></span>
						</div>
						<p>
						<div class="col-md-12 col-lg-12 text-left">
							<span><b>Data: </b><?php echo date("d/m/Y", strtotime($pagamento['data_pagamento']))."."; ?></span>
						</div>
						<p>
						<div class="col-md-12 col-lg-12 text-left">
							<span><b>Forma: </b><?php echo $pagamento['tipo_pagamento']."."; ?></span>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-lg-12 text-right">
							<a href="<?php echo base_url('area-adm/pagamento/editar/'.$pagamento['id_pagamento']); ?>" class="btn btn-warning">
								Editar <img src="<?php echo base_url('publico/imagens/editar.png'); ?>"/>
							</a>

							<a href="<?php echo base_url('pagamento/deletar/'.$pagamento['id_pagamento']); ?>" 
								onclick="return confirm('Deseja remover esse registro?');"class="btn btn-danger">
								Deletar <img src="<?php echo base_url('publico/imagens/deletar.png'); ?>"/>
							</a>
						</div>
					</div>
				</div>
			<?php } ?>			
		</div>
	</div>
</div>