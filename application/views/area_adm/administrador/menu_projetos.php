<?php 
  $URI = preg_replace('/\/$|\/r2geotec\//i', '', $_SERVER['REQUEST_URI']);
  $andamento = strpos($URI, 'andamento') !== FALSE;
  $parados = strpos($URI, 'parado') !== FALSE;
  $concluidos = strpos($URI, 'concluido') !== FALSE;
?>

<div class="row">
	
	<nav class="hidden-xs hidden-sm col-md-2 col-lg-2 col-md-offset-1 col-lg-offset-1">
		<ul class="nav nav-pills nav-stacked menu-lateral">
		  <li class="<?php echo ($andamento ? ' active' : '') ?>">
		    <a class="<?php if( $projetos_andamento == 0 ) echo 'link-bloqueado';?>" href="<?php if( $projetos_andamento != 0 ) echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/andamento/'.$usuario['id_usuario']); ?>"><b>Proj. em Andamento </b>(<?php echo $projetos_andamento ?>)</a>
		  </li>

		  <li class="<?php echo ($parados ? ' active' : '') ?>">
		    <a class="<?php if( $projetos_parados == 0 ) echo 'link-bloqueado';?>" href="<?php if( $projetos_parados != 0 ) echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/parado/'.$usuario['id_usuario']); ?>"><b>Projetos Parados </b>(<?php echo $projetos_parados ?>)</a>
		  </li>
		  
		  <li class="<?php echo ($concluidos ? ' active' : '') ?>">
		    <a class="<?php if( $projetos_concluidos == 0 ) echo 'link-bloqueado';?>" href="<?php if( $projetos_concluidos != 0 ) echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/concluido/'.$usuario['id_usuario']); ?>"><b>Projetos Concluídos </b>(<?php echo $projetos_concluidos ?>)</a>
		  </li>
		</ul>
	</nav>