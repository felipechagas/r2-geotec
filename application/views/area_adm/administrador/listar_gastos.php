		
		<div class="row">
			<?php foreach ($gastos as $gasto) { ?>
				<p>
				<div class="row destaque-linha">
					<p>
					<div class="row">
						<div class="text-left">
							<b>Dia </b><?php echo date("d/m/Y", strtotime($gasto['data']))."."; ?>
						</div>
					</div>
					<p>
					<div class="row">
						<div class="text-left">
							Estimativa de <b>gastos totais</b> é de <?php echo "<b>R$ ".$gasto['gasto_total']."</b>. Onde foram gastos:"; ?>
						</div>
					</div>
					<p>
					<div class="row">
						<div class="text-left">
							<b>Transporte</b>: <?php echo "R$ ".($gasto['distancia'])."."; ?>
							(<?php echo substr(($gasto['distancia']/$gasto['gasto_total'])*100, 0, 4); ?>%)
						</div>
					</div>
					<p>
					<div class="row">
						<div class="text-left">
							<b>Alimentação</b>: <?php echo "R$ ".($gasto['alimentacao'])."."; ?>
							(<?php echo substr(($gasto['alimentacao']/$gasto['gasto_total'])*100, 0, 4); ?>%)
						</div>
					</div>

					<?php foreach ($gasto['materiais'] as $material) { ?>
						<p>
						<div class="row">
							<div class="text-left">
								<b><?php echo $material['nome'] ?></b>: <?php echo "R$ ".($material['valor_hora']*$gasto['horas'])."."; ?>
								(<?php echo substr(($material['valor_hora']*$gasto['horas']/$gasto['gasto_total'])*100, 0, 4); ?>%)
							</div>
						</div>
					<?php } ?>

					<?php if($gasto['outros']>0) { ?>
						<p>
						<div class="row">
							<div class="text-left">
								<b>Outros</b>: <?php echo "R$ ".($gasto['outros'])."."; ?>
								(<?php echo substr(($gasto['outros']/$gasto['gasto_total'])*100, 0, 4); ?>%)
							</div>
						</div>
					<?php } ?>

					<div class="row">
						<div class="text-right">
							<a href="<?php echo base_url('gasto/deletar/'.$gasto['id_gasto']); ?>" 
								onclick="return confirm('Deseja remover esse registro?');"class="btn btn-danger">
								Deletar <img src="<?php echo base_url('publico/imagens/deletar.png'); ?>"/>
							</a>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>