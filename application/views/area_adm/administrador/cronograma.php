<div class="col-md-8 col-lg-8">
	<div class="row">
		<div class="col-md-4 col-lg-4 fundo-azul">
			<h5>Data de Início</h5>
		</div>
		<div class="col-md-4 col-lg-4 fundo-azul">
			<h5>Previsão de Entrega</h5>
		</div>
		<div class="col-md-4 col-lg-4 fundo-azul">
			<h5>Data de Conclusão</h5>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-lg-4 cronograma">
			<h5><?php echo date("d/m/Y", strtotime($cronograma['data_inicio'])) ?></h5>
		</div>
		
		<div class="col-md-4 col-lg-4 cronograma">
			<?php if($cronograma['previsao']=="0000-00-00 00:00:00") { ?>
				<form action="<?php echo base_url('area-adm/cronograma/previsao/'.$cronograma['id_projeto']); ?>" method="POST">
					<div class="col-md-10 col-lg-10">
						<div class="form-group row previsao">
							<div class="col-md-1 col-lg-1">
							</div>
				            <div class="col-md-11 col-lg-11 data_container">
				                <div class='input-group date' id='datetimepicker1'>
				                    <input type='text' class="form-control" value="<?php echo set_value('previsao'); ?>" name="previsao" placeholder="Escolha a data."/>
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>
				            </div>
				    	</div>
				    </div>
				    <div class="col-md-2 col-lg-2">
						<button type="submit" class="btn btn-success">
							Ir
						</button>
					</div>
				</form>
			<?php } else { ?>
				<h5><?php echo date("d/m/Y", strtotime($cronograma['previsao'])) ?></h5>
			<?php } ?>
		</div>
		
		<div class="col-md-4 col-lg-4 cronograma">
			<?php if($cronograma['data_entrega']=="0000-00-00 00:00:00") { ?>
				<a href="<?php echo base_url('area-adm/concluir-projeto/'.$cronograma['id_projeto']); ?>" class="btn btn-success">
					Definir Como Concluído
				</a>
			<?php } else { ?>
				<h5><?php echo date("d/m/Y", strtotime($cronograma['data_entrega'])) ?></h5>
			<?php } ?>
		</div>
	</div>
</div>