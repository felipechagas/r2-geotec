		<div class="row">
			<?php foreach ($relatorios as $relatorio) { ?>
				<?php if(!$relatorio['compartilhado']) continue; ?>
				<p>
				<div class="row destaque-linha">
					<div class="row">
						<p>
						<div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1 text-left">
							<span><b>Dia </b><?php echo date("d/m/Y", strtotime($relatorio['data_relatorio']))."."; ?></span>
						</div>
						<p>
						<div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1  text-left">
							<span><b>Relatório: </b><?php echo $relatorio['descricao']."."; ?></span>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>