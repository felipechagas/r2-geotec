<div class="row">
	<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
		<div class="row">
			<div class="col-md-12 col-lg-12 dados-clientes">
				<div class="row fundo-azul">
					<h4>
						Meus Dados
					</h4>
				</div>
				<div class="row dados-clientes-padding">
					<div class="row dados-clientes-linha">
						<div class="col-md-12 col-lg-12 text-left">
							<span><b>Nome:</b> <?php echo $usuario['nome'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col-md-5 col-lg-5 text-left">
							<span><b>CNPJ:</b> <?php echo $usuario['cnpj'].'.'; ?></span>
						</div>
						<div class="col-md-6 col-lg-6 text-left">
							<span><b>CPF:</b> <?php echo $usuario['cpf'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col-md-5 col-lg-5 text-left">
							<span><b>E-mail:</b> <?php echo $usuario['email'].'.'; ?></span>
						</div>
						<div class="col-md-3 col-lg-3 text-left">
							<span><b>Fixo:</b> <?php echo $usuario['telefone_1'].'.'; ?></span>
						</div>
						<div class="col-md-4 col-lg-4 text-right">
							<span><b>Cel.:</b> <?php echo $usuario['telefone_2'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col-md-5 col-lg-5 text-left">
							<span><b>Cidade:</b> <?php echo $usuario['cidade'].' - '.$usuario['estado'].'.'; ?></span>
						</div>
						<div class="col-md-3 col-lg-3 text-left">
							<span><b>CEP:</b> <?php echo $usuario['cep'].'.'; ?></span>
						</div>
					</div>
					<div class="row dados-clientes-linha">
						<div class="col-md-8 col-lg-8 text-left">
							<span><b>Endereço:</b> <?php echo $usuario['logradouro'].', '.$usuario['numero'].'. '.$usuario['bairro'].'.'; ?></span>
						</div>
						<div class="col-md-4 col-lg-4 text-left">
							<span><b>Comp.:</b> <?php echo $usuario['complemento'].'.'; ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>