<div class="row">
	<div class="contatos-titulo col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
		<div class="row">
			<h4 class="hidden-md hidden-lg"><b>Fale Conosco</b></h4>
          	<h2 class="hidden-xs hidden-sm">Fale Conosco</h2>
		</div>
		<div class="row contatos-centro">
			<div class="contatos-centro col-xs-10 col-sm-10 col-md-4 col-lg-4 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">	
				<div class="row">
					<div class="col-12 text-left">
						<b>Rogério Gadelha: Diretor Geral/Topógrafo.</b>
					</div>
					<div class="col-12 text-left">
						E-mail: R2geotec@gmail.com.
					</div>
					<div class="col-12 text-left">
						Whatsapp: (85) 9 8848-0470.
					</div>
					<div class="col-12 text-left">
						Telefone: (85) 9 9915-4626.
					</div>
				</div>
			</div>

			<div class="contatos-centro col-xs-10 col-sm-10 col-md-6 col-lg-6 col-xs-offset-1 col-sm-offset-1">
				<div class="row">
					<h4 class="hidden-md hidden-lg"><b>Onde Nos Encontrar?</b></h4>
		          	<h4 class="hidden-xs hidden-sm">Onde Nos Encontrar?</h4>
				</div>

				<div class="row">
					<div id="mapa" style="height: 300px; width: 100%;"></div>
				</div>
				<br>
			</div>
		</div>
	</div>
</div>