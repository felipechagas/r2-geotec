<div class="container slide-container">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
	    <!-- Indicators -->
	    <ol class="carousel-indicators hidden-xs hidden-sm">
	      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	      <li data-target="#myCarousel" data-slide-to="1"></li>
	      <li data-target="#myCarousel" data-slide-to="2"></li>
	      <li data-target="#myCarousel" data-slide-to="3"></li>
	    </ol>

	    <!-- Wrapper for slides -->
	    <div class="carousel-inner" role="listbox">
	      <div class="item active">
	        <img src="<?php echo base_url('publico/imagens/slides/home/slide-1.jpg')?>" alt="slid-1" width="100%">
	      </div>
	      <div class="item">
		    <img src="<?php echo base_url('publico/imagens/slides/home/slide-2.jpg')?>" alt="" width="100%">
		  </div>
		  <div class="item">
		    <img src="<?php echo base_url('publico/imagens/slides/home/slide-3.jpg')?>" alt="" width="100%">
		  </div>
		  <div class="item">
		    <img src="<?php echo base_url('publico/imagens/slides/home/slide-4.jpg')?>" alt="" width="100%">
		  </div>
	    </div>


	    <!-- Left and right controls -->
	    <a class="left carousel-control controle-slide" href="#myCarousel" role="button" data-slide="prev">
	      <span class="" aria-hidden="true"></span>
	      <span class="sr-only">Próximo</span>
	    </a>
	    <a class="right carousel-control controle-slide" href="#myCarousel" role="button" data-slide="next">
	      <span class="" aria-hidden="true"></span>
	      <span class="sr-only">Anterior</span>
	    </a>
	</div>

	<div class="row">
	 	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 faixa-azul" >
	 		<p>
		 		A <img width="80px" src="<?php echo base_url("publico/imagens/logo-r2-geotec-pequena.png") ?>"> oferece solução em Topografia, Licenciamento Ambiental, Projetos de Edificação e muito mais.<br>
		 		Leia mais sobre nossos serviços e entre em contato para um orçamento.
		 	</p>
	 	</div>
	</div>

	<div class="row" id="inf-home">
		<div class="inf-home-caixa col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="row">
				<div class="inf-home-item col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<a href="<?php echo base_url('servicos/georeferenciamento'); ?>">
						<div class="row">
							<img src="<?php echo base_url('publico/imagens/topografia-home.png')?>" alt="" width="100%">
						</div>
					</a>
					<div class="row caixa-texto-home-azul altura-adaptador">
						<a href="<?php echo base_url('servicos/georeferenciamento'); ?>">
							<div class="row">
								<h3>Topografia</h3>
							</div>
						</a>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
								<p class="text-justify text-paragrafo  hidden-xs hidden-sm">
									No intuito de atender às mais rigorosas normas voltadas à topografia clássica e ao georreferenciamento, a R2 Geotec dispõe de equipamentos de alta precisão e uma equipe qualificada para atender nossos clientes. A empresa atua em projetos de engenharia nas áreas de edificações, estradas, terraplenagem, drenagem, etc, bem como nos  processos de regularização fundiária e alterações de imóveis, como  desmembramentos, retificação de área, remembramento, loteamento, etc.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="inf-home-caixa col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="row">
				<div class="inf-home-item col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<a href="<?php echo base_url('servicos/licenciamento'); ?>">
						<div class="row">
							<img src="<?php echo base_url('publico/imagens/licenciamento-ambiental.png')?>" alt="" width="100%">
						</div>
					</a>
					<div class="row caixa-texto-home-vermelho altura-adaptador">
						<a href="<?php echo base_url('servicos/licenciamento'); ?>">
							<div class="row">
								<h3>Licenciamento Ambiental</h3>
							</div>
						</a>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
								<p class="text-justify text-paragrafo  hidden-xs hidden-sm">
									A R2 Geotec, atenta ao crescimento das preocupações com as questões ambientais e as subsequentes exigências do governo em relação às mesmas, visa colaborar para um desenvolvimento sustentável e dentro das normas ambientais. Nesse cenário, a empresa oferece soluções na submissão de projetos aos órgãos de controle ambiental, sejam empreendimentos voltados à edificação, agrícola, loteamentos, residenciais, etc. Preparamos todos os arquivos e projetos topográficos exigidos para obtenção da licença.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="inf-home-caixa col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="row">
				<div class="inf-home-item col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<a href="<?php echo base_url('servicos/comerciais'); ?>">
						<div class="row">
							<img src="<?php echo base_url('publico/imagens/projetos-edificacao.png')?>" alt="" width="100%">
						</div>
					</a>
					<div class="row caixa-texto-home-azul altura-adaptador">
						<a href="<?php echo base_url('servicos/comerciais'); ?>">
							<div class="row">
								<h3>Projetos de Edificação</h3>
							</div>
						</a>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
								<p class="text-justify text-paragrafo hidden-xs hidden-sm">
									Iniciar uma obra, ainda que de pequeno porte, sem um planejamento prévio é um grande risco de insucesso. Atrasos, excesso de gastos e embargos são alguns dos problemas inerentes de uma obra sem planejamento ou mal planejada.  Nesse contexto, a R2 Geotec disponibiliza o serviço de elaboração de plantas (arquitetônica, hidrossanitária, situação, etc.) e arquivos (shape, kml, memoriais, etc.) visando à tranquilidade e satisfação do cliente na entrega de um projeto eficiente e eficaz nos quesitos estruturais e legais.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="inf-home-caixa col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="row">
				<div class="inf-home-item col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<a href="<?php echo base_url('servicos/car'); ?>">
						<div class="row">
							<img src="<?php echo base_url('publico/imagens/car.png')?>" alt="" width="100%">
						</div>
					</a>
					<div class="row caixa-texto-home-vermelho" id="mais-alta">
						<a href="<?php echo base_url('servicos/car'); ?>">
							<div class="row">
								<h3>Cadastro Ambiental Rural</h3>
							</div>
						</a>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
								<p class="text-justify text-paragrafo  hidden-xs hidden-sm">
									Criado pela lei nº 12.651/2012, no âmbito do sistema nacional de informação  sobre meio ambiente - sinima, e regulamentado pela instrução normativa mma nº 2,   de 5 de maio de 2014, o cadastro ambiental rural – car é um registro público  eletrônico de âmbito nacional, obrigatório para todos os imóveis rurais.  A inscrição no car é o primeiro passo para obtenção da regularidade ambiental do imóvel. A R2 Geotec oferece apoio ao cliente em todas as etapas desse processo, desde os levantamentos necessários até o cadastro dos formulários.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

