<div class="row">
  <div class="portifolio col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
    <div class="portifolio-titulo col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="text-center row">
        <h4 class="hidden-md hidden-lg"><b>Veja Alguns dos Nossos Projetos</b></h4>
        <h2 class="hidden-xs hidden-sm">Veja Alguns dos Nossos Projetos</h2>
      </div>
      <div class="text-justify row">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
          <p class="text-justify">
            Sed ut perspiciatis unde omnis iste natus error sit
            voluptatem accusantium doloremque laudantium, totam rem
            aperiam, eaque ipsa quae ab illo inventore veritatis
            et quasi architecto beatae vitae dicta sunt explicabo.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

