<div class="col-md-8 col-lg-8">
	<div class="row">
		<div class="col-md-12 col-lg-12">
			<form action="<?php echo base_url('area-adm/adicionar-gasto/'.$projeto['id_projeto']); ?>" method="POST" enctype="multipart/form-data">
			    
			    <div class="form-group row">
			        <div class="text-center col-md-2 col-lg-2">
			            <label>Distância: </label>
			        </div>
			        <div class="col-md-4 col-lg-4">
			            <div class="row">
			              <input type="text" class="form-control" value="<?php echo set_value('distancia'); ?>" name="distancia" placeholder="Distância do projeto em Km.">
			            </div>
			            <?php echo form_error('distancia'); ?>
			        </div>

			        <div class="text-center col-md-2 col-lg-2">
			            <label>Alimentação: </label>
			        </div>
			        <div class="col-md-4 col-lg-4">
			            <div class="row">
			              <input type="text" class="form-control" value="<?php echo set_value('alimentacao'); ?>" name="alimentacao" placeholder="Gasto com alimentação em R$.">
			            </div>
			            <?php echo form_error('alimentacao'); ?>
			        </div>
			    </div>

			    <div class="form-group row">
			        <div class="text-center col-md-2 col-lg-2">
			            <label>Horas de Trabalho: </label>
			        </div>
			        <div class="col-md-4 col-lg-4">
			            <div class="row">
			              <input type="text" class="form-control" value="<?php echo set_value('horas'); ?>" name="horas" placeholder="Tempo de trabalho em horas.">
			            </div>
			            <?php echo form_error('horas'); ?>
			        </div>

			        <div class="text-center col-md-2 col-lg-2">
			            <label>Outros Gastos: </label>
			        </div>
			        <div class="col-md-4 col-lg-4">
			            <div class="row">
			              <input type="text" class="form-control" value="<?php echo set_value('outros'); ?>" name="outros" placeholder="Soma de outros gasto em R$.">
			            </div>
			            <?php echo form_error('outros'); ?>
			        </div>
			    </div>

			    <div class="form-group row">
			    	<div class="row" >
				    	<div class="text-center col-md-2 col-lg-2">
				            <label>Materiais: </label>
				        </div>
				    </div>
					<div class="form-check row">
						<?php foreach ($materiais as $material) { ?>
							<div class="col-md-4 col-lg-4">
								<div class="row">
									<div class="col-md-9 col-lg-9 text-right">
							    		<?php echo $material['nome']; ?>
							    	</div>
							    	<div class="col-md-1 col-lg-1 text-center">
							        	<input type="checkbox" name="<?php echo $material['id_material']; ?>" class="form-check-input">
									</div>
								</div>
							</div>
						<?php } ?>
						<div class="col-md-4 col-lg-4">
							<div class="col-md-9 col-lg-9">
								<input type="text" class="form-control" value="<?php echo set_value('novo_material'); ?>" name="novo_material" placeholder="Outro Material.">
							</div>
							<div class="col-md-3 col-lg-3">
								<input type="text" class="form-control" value="<?php echo set_value('novo_valor'); ?>" name="novo_valor" placeholder="Valor.">
							</div>
							<?php echo form_error('novo_material'); ?>
						</div>
					</div>
				</div>

			    <div class="row">
				    <button type="submit" class="botao-login btn btn-info btn-sm">Adicionar</button>
				</div>
			</form>
		</div>
	</div>