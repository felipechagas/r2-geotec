<div class="col-md-8 col-lg-8">
	
	<?php if($_SESSION['nivel_usuario']=="administrador" || $_SESSION['nivel_usuario']=="funcionario") { ?>

		<div class="row">
			<div class="col-md-12 col-lg-12">
				<form action="<?php echo base_url('area-adm/adicionar-historico/'.$projeto['id_projeto']); ?>" method="POST" enctype="multipart/form-data">
				    
				    <div class="form-group row">
				        <div class="text-right col-md-3 col-lg-3">
				            <label>Relatório: </label>
				        </div>
				        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
				            <div class="row text-right">
				              <input type="text" class="form-control" value="<?php echo set_value('descricao'); ?>" name="descricao" placeholder="Relatório de atividades realizadas.">
				            </div>
				            <?php echo form_error('descricao'); ?>
				        </div>
				    </div>

				    <div class="form-group row">
						<div class="form-check text-right">
						    <label class="form-check-label">
						        <input type="checkbox" name="compartilhado" class="form-check-input">
						        Compartilhar com cliente
						    </label>
						</div>
					</div>

				    <div class="row">
					    <button type="submit" class="botao-login btn btn-info btn-sm">Adicionar</button>
					</div>
				</form>
			</div>
		</div>

	<?php } ?>