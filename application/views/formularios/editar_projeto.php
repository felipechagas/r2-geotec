	<div class="col-md-6 col-lg-6 col-md-offset-1 col-lg-offset-1">
		<form method="POST" action="<?php echo base_url('area-adm/editar/projeto/'.$projeto['id_projeto']); ?>">

		  <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Data de Início: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right data_container">
	          	<div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" value="<?php echo set_value('data_inicio'); ?>" name="data_inicio" placeholder="Escolha a data de início."/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
	          </div>
	        </div>
	      </div>
		  	
	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Descrição: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input id="descricao_projeto" type="text" class="form-control" value="<?php echo set_value('descricao_projeto'); ?>" name="descricao_projeto" placeholder="Descrever o projeto de forma breve.">
	          </div>
	          <?php echo form_error('descricao_projeto'); ?>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Status: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <select name="status" class="form-control" >
				<option value="andamento" <?php echo  set_select('status', 'andamento', TRUE); ?>>Em Andamento</option>
				<option value="concluido" <?php echo  set_select('status', 'concluido'); ?>>Concluído</option>
				<option value="parado" <?php echo  set_select('status', 'parado'); ?>>Parado</option>
			  </select>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Logradouro: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('logradouro'); ?>" name="logradouro" placeholder="Nome da Rua, Avenida, Estrada, ETC.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Numero: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('numero'); ?>" name="numero" placeholder="Numero do endereço comercial ou residencial.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Bairro: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('bairro'); ?>" name="bairro" placeholder="Nome do bairro sem abreviação.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Complemento: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('complemento'); ?>" name="complemento" placeholder="Complemento do endereço.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Cidade: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('cidade'); ?>" name="cidade" placeholder="Cidade do cliente sem abreviar.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Estado: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('estado'); ?>" name="estado" placeholder="Estado do cliente sem abreviar.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>CEP: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" id="cep" class="form-control" value="<?php echo set_value('cep'); ?>" name="cep" placeholder="CEP no formato 00.000-000.">
	          </div>
	        </div>
	      </div>

		  <div class="row">
		    <button type="submit" class="botao-login btn btn-info btn-sm">Editar</button>
		  </div>

	  </form>
	</div>

</div>