	<div class="col-md-6 col-lg-6 col-md-offset-1 col-lg-offset-1">
		<form class="data_formato" method="POST" action="<?php echo base_url('area-adm/adicionar-projeto/'.$usuario['id_usuario']); ?>">
	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Nome do Projeto: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('nome_projeto'); ?>" name="nome_projeto" placeholder="Tentar escolher um nome pequeno e intuitivo.">
	          </div>
	          <?php echo form_error('nome_projeto'); ?>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Data de Início: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right data_container">
	          	<div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" value="<?php echo set_value('data_inicio'); ?>" name="data_inicio" placeholder="Escolha a data de início."/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Descrição: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input id="descricao_projeto" type="text" class="form-control" value="<?php echo set_value('descricao_projeto'); ?>" name="descricao_projeto" placeholder="Descrever o projeto de forma breve.">
	          </div>
	          <?php echo form_error('descricao_projeto'); ?>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Status: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <select name="status" class="form-control" >
				<option value="andamento" <?php echo  set_select('status', 'andamento', TRUE); ?>>Em Andamento</option>
				<option value="concluido" <?php echo  set_select('status', 'concluido'); ?>>Concluído</option>
				<option value="parado" <?php echo  set_select('status', 'parado'); ?>>Parado</option>
			  </select>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Logradouro: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('logradouro'); ?>" name="logradouro" placeholder="Nome da Rua, Avenida, Estrada, ETC.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Numero: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('numero'); ?>" name="numero" placeholder="Numero do endereço comercial ou residencial.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Bairro: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('bairro'); ?>" name="bairro" placeholder="Nome do bairro sem abreviação.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Complemento: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('complemento'); ?>" name="complemento" placeholder="Complemento do endereço.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Cidade: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('cidade'); ?>" name="cidade" placeholder="Cidade do cliente sem abreviar.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Estado: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('estado'); ?>" name="estado" placeholder="Estado do cliente sem abreviar.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>CEP: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" id="cep" class="form-control" value="<?php echo set_value('cep'); ?>" name="cep" placeholder="CEP no formato 00.000-000.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Cartorio: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <select name="usuario_1" class="form-control chosen-select" >
	          	<option value="" <?php echo  set_select('usuario_1', '', TRUE); ?>>--Escolha o cartório responsável--</option>
				<?php foreach ($usuarios['cartorios'] as $cartorio) { ?>
				  <option value="<?php echo $cartorio['id_usuario'] ?>" <?php echo  set_select('usuario_1', $cartorio['id_usuario']); ?> ><?php echo $cartorio['nome'] ?></option>
				<?php } ?>
			  </select>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Prefeitura: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <select name="usuario_2" class="form-control chosen-select" >
	          	<option value="" <?php echo  set_select('usuario_2', '', TRUE); ?>>--Escolha a prefeitura responsável--</option>
				<?php foreach ($usuarios['prefeituras'] as $cartorio) { ?>
				  <option value="<?php echo $cartorio['id_usuario'] ?>" <?php echo  set_select('usuario_2', $cartorio['id_usuario']); ?> ><?php echo $cartorio['nome'] ?></option>
				<?php } ?>
			  </select>
	        </div>
	      </div>

	      <!-- Adicionar outros clientes ao projeto -->
	      <div class="row btn_clientes_adicionais">
	      	<div class="col-md-8 col-lg-8 col-md-offset-4 col-lg-offset-4">
	      		<a class="btn btn-info" id="btn_clientes_adicionais" ><b>+</b> Adicionar Outros Cliente a Este Projeto</a>
	      	</div>
	      </div>

	      <div class="clientes_adicionais" id="clientes_adicionais">
		      <div class="form-group row">
		        <div class="text-right col-md-3 col-lg-3">
		          <label>Cliente 1: </label>
		        </div>

		        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
		          <select name="usuario_3" class="form-control chosen-select" >
		          	<option value="" <?php echo  set_select('usuario_3', '', TRUE); ?>>--Escolha outro cliente para este projeto--</option>
					<?php foreach ($usuarios['clientes'] as $cartorio) { ?>
					  <option value="<?php echo $cartorio['id_usuario'] ?>" <?php echo  set_select('usuario_3', $cartorio['id_usuario']); ?> ><?php echo $cartorio['nome'] ?></option>
					<?php } ?>
				  </select>
		        </div>
		      </div>

		      <div class="form-group row">
		        <div class="text-right col-md-3 col-lg-3">
		          <label>Cliente 2: </label>
		        </div>

		        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
		          <select name="usuario_4" class="form-control chosen-select" >
		          	<option value="" <?php echo  set_select('usuario_4', '', TRUE); ?>>--Escolha outro cliente para este projeto--</option>
					<?php foreach ($usuarios['clientes'] as $cartorio) { ?>
					  <option value="<?php echo $cartorio['id_usuario'] ?>" <?php echo  set_select('usuario_4', $cartorio['id_usuario']); ?> ><?php echo $cartorio['nome'] ?></option>
					<?php } ?>
				  </select>
		        </div>
		      </div>

		      <div class="form-group row">
		        <div class="text-right col-md-3 col-lg-3">
		          <label>Cliente 3: </label>
		        </div>

		        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
		          <select name="usuario_5" class="form-control chosen-select" >
		          	<option value="" <?php echo  set_select('usuario_5', '', TRUE); ?>>--Escolha outro cliente para este projeto--</option>
					<?php foreach ($usuarios['clientes'] as $cartorio) { ?>
					  <option value="<?php echo $cartorio['id_usuario'] ?>" <?php echo  set_select('usuario_5', $cartorio['id_usuario']); ?> ><?php echo $cartorio['nome'] ?></option>
					<?php } ?>
				  </select>
		        </div>
		      </div>

		      <div class="form-group row">
		        <div class="text-right col-md-3 col-lg-3">
		          <label>Cliente 4: </label>
		        </div>

		        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
		          <select name="usuario_6" class="form-control chosen-select" >
		          	<option value="" <?php echo  set_select('usuario_6', '', TRUE); ?>>--Escolha outro cliente para este projeto--</option>
					<?php foreach ($usuarios['clientes'] as $cartorio) { ?>
					  <option value="<?php echo $cartorio['id_usuario'] ?>" <?php echo  set_select('usuario_6', $cartorio['id_usuario']); ?> ><?php echo $cartorio['nome'] ?></option>
					<?php } ?>
				  </select>
		        </div>
		      </div>

		      <div class="form-group row">
		        <div class="text-right col-md-3 col-lg-3">
		          <label>Cliente 5: </label>
		        </div>

		        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
		          <select name="usuario_7" class="form-control chosen-select" >
		          	<option value=""  <?php echo  set_select('usuario_7', '', TRUE); ?>>--Escolha outro cliente para este projeto--</option>
					<?php foreach ($usuarios['clientes'] as $cartorio) { ?>
					  <option value="<?php echo $cartorio['id_usuario'] ?>" <?php echo  set_select('usuario_7', $cartorio['id_usuario']); ?> ><?php echo $cartorio['nome'] ?></option>
					<?php } ?>
				  </select>
		        </div>
		      </div>

		      <div class="form-group row">
		        <div class="text-right col-md-3 col-lg-3">
		          <label>Cliente 6: </label>
		        </div>

		        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
		          <select name="usuario_8" class="form-control chosen-select" >
		          	<option value="" <?php echo  set_select('usuario_8', '', TRUE); ?>>--Escolha outro cliente para este projeto--</option>
					<?php foreach ($usuarios['clientes'] as $cartorio) { ?>
					  <option value="<?php echo $cartorio['id_usuario'] ?>" <?php echo  set_select('usuario_8', $cartorio['id_usuario']); ?> ><?php echo $cartorio['nome'] ?></option>
					<?php } ?>
				  </select>
		        </div>
		      </div>

		      <div class="form-group row">
		        <div class="text-right col-md-3 col-lg-3">
		          <label>Cliente 7: </label>
		        </div>

		        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
		          <select name="usuario_9" class="form-control chosen-select" >
		          	<option value="" <?php echo  set_select('usuario_9', '', TRUE); ?>>--Escolha outro cliente para este projeto--</option>
					<?php foreach ($usuarios['clientes'] as $cartorio) { ?>
					  <option value="<?php echo $cartorio['id_usuario'] ?>" <?php echo  set_select('usuario_9', $cartorio['id_usuario']); ?> ><?php echo $cartorio['nome'] ?></option>
					<?php } ?>
				  </select>
		        </div>
		      </div>

		      <div class="form-group row">
		        <div class="text-right col-md-3 col-lg-3">
		          <label>Cliente 8: </label>
		        </div>

		        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
		          <select name="usuario_10" class="form-control chosen-select" >
		          	<option value="" <?php echo  set_select('usuario_10', '', TRUE); ?>>--Escolha outro cliente para este projeto--</option>
					<?php foreach ($usuarios['clientes'] as $cartorio) { ?>
					  <option value="<?php echo $cartorio['id_usuario'] ?>" <?php echo  set_select('usuario_10', $cartorio['id_usuario']); ?> ><?php echo $cartorio['nome'] ?></option>
					<?php } ?>
				  </select>
		        </div>
		      </div>

		      <div class="form-group row">
		        <div class="text-right col-md-3 col-lg-3">
		          <label>Cliente 9: </label>
		        </div>

		        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
		          <select name="usuario_11" class="form-control chosen-select" >
		          	<option value="" <?php echo  set_select('usuario_11', '', TRUE); ?>>--Escolha outro cliente para este projeto--</option>
					<?php foreach ($usuarios['clientes'] as $cartorio) { ?>
					  <option value="<?php echo $cartorio['id_usuario'] ?>" <?php echo  set_select('usuario_11', $cartorio['id_usuario']); ?> ><?php echo $cartorio['nome'] ?></option>
					<?php } ?>
				  </select>
		        </div>
		      </div>
		  </div>

		  <div class="row">
		    <button type="submit" class="botao-login btn btn-info btn-sm">Adicionar</button>
		  </div>

	  </form>
	</div>

</div>