<div class="col-md-8 col-lg-8">
	<div class="row">
		<div class="col-md-12 col-lg-12">
			<form action="<?php echo base_url('area-adm/pagamento/editar/'.$pagamento['id_pagamento']); ?>" method="POST" enctype="multipart/form-data">
			    
			    <div class="form-group row">
			        <div class="text-right col-md-3 col-lg-3">
			            <label>Valor Pago: </label>
			        </div>
			        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
			            <div class="row text-right">
			              <input type="text" class="form-control" value="<?php echo set_value('valor_recebido'); ?>" name="valor_recebido" placeholder="Valor pago em reais (R$).">
			            </div>
			            <?php echo form_error('valor_recebido'); ?>
			        </div>
			    </div>

			    <div class="form-group row">
			        <div class="text-right col-md-3 col-lg-3">
			            <label>Forma de Pagamento: </label>
			        </div>
			        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
			            <div class="row text-right">
			              <input type="text" class="form-control" value="<?php echo set_value('tipo_pagamento'); ?>" name="tipo_pagamento" placeholder="Ex.: Dinheiro, Cheque, Lotes, Terrenos, etc.">
			            </div>
			            <?php echo form_error('tipo_pagamento'); ?>
			        </div>
			    </div>

			    <div class="row">
				    <button type="submit" class="botao-login btn btn-info btn-sm">Editar</button>
				</div>
			</form>
		</div>
	</div>
</div>