<div class="col-md-8 col-lg-8">

	<?php if($_SESSION['nivel_usuario']=="administrador" || $_SESSION['nivel_usuario']=="funcionario") { ?>

		<div class="row">
			<div class="col-md-12 col-lg-12">
				<form action="<?php echo base_url('area-adm/adicionar-arquivos/'.$projeto['id_projeto']); ?>" method="POST" enctype="multipart/form-data">
				    
					<input type="text" class="hidden" value="<?php echo $projeto['id_projeto']; ?>"  name="id">

				    <div class="form-group row">
				        <div class="text-right col-md-1 col-lg-1">
				            <label>Nome: </label>
				        </div>
				        <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
				            <div class="row text-right">
				              <input type="text" class="form-control" value="<?php echo set_value('nome'); ?>" name="nome" placeholder="Nome pequeno e intuitivo para o arquivo.">
				            </div>
				            <?php echo form_error('nome'); ?>
				        </div>
				    </div>
				    
				    <div class="form-group row">
				    	<div class="text-right col-md-1 col-lg-1">
				            <label>Arquivo: </label>
				        </div>
				    	<div class="col-md-6 col-lg-6 col-md-offset-1 col-lg-offset-1 text-left">
							<div class="row">
								<div class="col-md-12 col-lg-12 text-left">
								    <input type="file" value="<?php echo set_value('arquivo'); ?>" name="arquivo" class="form-control-file" aria-describedby="fileHelp">
								   	<small id="fileHelp" class="form-text text-muted">Evite subir arquivos editáveis (.doc, .dwg, .ptt, etc.).</small>
								</div>
							</div>
							<?php echo form_error('arquivo'); ?>
						</div>
						<div class="col-md-4 col-lg-4 text-right">
							<div class="text-right col-md-2 col-lg-2">
					          <label>Tipo: </label>
					        </div>

							<div class="col-md-9 col-lg-9 col-md-offset-1 col-lg-offset-1">
					          <select name="tipo" class="form-control" >
								<option value="fechado" <?php echo  set_select('tipo', 'fechado', TRUE); ?>>Fechado</option>
								<option value="aberto" <?php echo  set_select('tipo', 'aberto'); ?>>Aberto</option>
								<option value="confidencial" <?php echo  set_select('tipo', 'confidencial'); ?>>Confidencial</option>
							  </select>
					        </div>
						</div>
						
					</div>
					<div class="col-md-2 col-lg-2 col-md-offset-10 col-lg-offset-10  text-right">
					    <button type="submit" class="botao-login btn btn-info btn-sm">Adicionar</button>
					</div>
				</form>
			</div>
		</div>
	<?php } ?>
