<div class="row">
	<div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
		<form method="POST" action="<?php echo base_url('area-adm/'.$_SESSION['nivel_usuario'].'/criar-senha/'.$id); ?>">
	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Crie uma senha: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="password" class="form-control" value="" name="senha" placeholder="Digite aqui sua senha.">
	          </div>
	          <?php echo form_error('senha'); ?>
	        </div>
	      </div>
	      
	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Repita a senha: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="password" class="form-control" value="" name="repetir_senha" placeholder="Digite novamente a senha escolhida.">
	          </div>
	          <?php echo form_error('repetir_senha'); ?>
	        </div>
	      </div>

	      <div class="row">
		    <button type="submit" class="botao-login btn btn-info btn-sm">Criar Senha</button>
		  </div>
	    </form>
	</div>
</div>