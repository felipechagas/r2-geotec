	<div class="col-md-6 col-lg-6 col-md-offset-1 col-lg-offset-1">
		<form method="POST" action="<?php echo base_url('area-adm/adicionar/'.$tipo); ?>">
	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Nome Completo: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('nome'); ?>" name="nome" placeholder="Digite o nome completo do cliente.">
	          </div>
	          <?php echo form_error('nome'); ?>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>E-mail: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('email'); ?>" name="email" placeholder="Digite o email do cliente.">
	          </div>
	          <?php echo form_error('email'); ?>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>CPF: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" id="cpf" class="form-control" value="<?php echo set_value('cpf'); ?>" name="cpf" placeholder="CPF no formato 000.000.000-00.">
	          </div>
	          <?php echo form_error('cpf'); ?>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>CNPJ: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" id="cnpj" class="form-control" value="<?php echo set_value('cnpj'); ?>" name="cnpj" placeholder="CNPJ no formato XX.XXX.XXX/0001-ZZ.">
	          </div>
	          <?php echo form_error('cnpj'); ?>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Telefone Fixo: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" id="telefone_fixo" class="form-control" value="<?php echo set_value('telefone_1'); ?>" name="telefone_1" placeholder="Telefone fixo com DDD.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Telefone Celular: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" id="telefone_celular" class="form-control" value="<?php echo set_value('telefone_2'); ?>" name="telefone_2" placeholder="Telefone celular com DDD.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Logradouro: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('logradouro'); ?>" name="logradouro" placeholder="Nome da Rua, Avenida, Estrada, ETC.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Numero: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('numero'); ?>" name="numero" placeholder="Numero do endereço comercial ou residencial.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Bairro: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('bairro'); ?>" name="bairro" placeholder="Nome do bairro sem abreviação.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Complemento: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('complemento'); ?>" name="complemento" placeholder="Complemento do endereço.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Cidade: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('cidade'); ?>" name="cidade" placeholder="Cidade do cliente sem abreviar.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>Estado: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" class="form-control" value="<?php echo set_value('estado'); ?>" name="estado" placeholder="Estado do cliente sem abreviar.">
	          </div>
	        </div>
	      </div>

	      <div class="form-group row">
	        <div class="text-right col-md-3 col-lg-3">
	          <label>CEP: </label>
	        </div>

	        <div class="col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
	          <div class="row text-right">
	            <input type="text" id="cep" class="form-control" value="<?php echo set_value('cep'); ?>" name="cep" placeholder="CEP no formato 00.000-000.">
	          </div>
	        </div>
	      </div>

		  <div class="row">
		    <button type="submit" class="botao-login btn btn-info btn-sm">Adicionar</button>
		  </div>

	  </form>
	</div>

</div>