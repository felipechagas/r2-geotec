<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="icon" href="<?php echo base_url(); ?>publico/imagens/fiveicon.ico" type="image/x-icon">
  <link rel="shortcut icon" href="<?php echo base_url(); ?>publico/imagens/fiveicon.ico" type="image/x-icon">

  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>R2 Geotec</title>

  <!-- Bootstrap -->
  <?php echo link_tag('publico/css/libs/bootstrap.css'); ?>

  <!-- Geral -->
  <?php echo link_tag('publico/css/geral.css'); ?>

  <!-- Menu -->
  <?php echo link_tag('publico/css/template/menu.css'); ?>

  <!-- Header -->
  <?php echo link_tag('publico/css/template/header.css'); ?>

  <!-- Inicio -->
  <?php echo link_tag('publico/css/home.css'); ?>

  <!-- Parceiros -->
  <?php echo link_tag('publico/css/parceiros.css'); ?>

  <!-- Inscricoes -->
  <?php echo link_tag('publico/css/inscricoes.css'); ?>

  <!-- Logo -->
  <?php echo link_tag('publico/css/template/logo.css'); ?>

  <!-- Menu ADM -->
  <?php echo link_tag('publico/css/menu-area-adm.css'); ?>

  <!-- Footer -->
  <?php echo link_tag('publico/css/template/footer.css'); ?>

  <!-- Servicos -->
  <?php echo link_tag('publico/css/servicos.css'); ?>

  <!-- CSS Mobile -->
  <?php echo link_tag('publico/css/mobile/mobile-portrait.css'); ?>
  <?php echo link_tag('publico/css/mobile/mobile-landscape.css'); ?>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <div class="header col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="hidden-xs hidden-sm col-md-4 col-lg-4 col-md-offset-1 col-lg-offset-1">
        <div class="logo-grande">
          <img src="<?php echo base_url('publico/imagens/logo-r2-geotec.png'); ?>" >
        </div>
    </div>

    <div class="formulario-login hidden-xs hidden-sm col-md-6 col-lg-6">
        <form>
            <div class="form-group row">
              <div class="col-md-6 col-lg-6">
                <label class="label-login" for="campo-usuario-1">Usuário: </label>
              </div>

              <div class="col-md-6 col-lg-6">
                <input type="email" class="form-control" id="campo-usuario-1" placeholder="Digite seu e-mail">
              </div>
            </div>

            <div class="form-group row">
              <div class="col-md-6 col-lg-6">
                <label class="label-login" for="campo-senha-1">Senha: </label>
              </div>

              <div class="col-md-6 col-lg-6">
                <input type="password" class="form-control" id="campo-senha-1" placeholder="Digite sua Senha">
              </div>
            </div>

          <div class="row">
            <button type="submit" class="botao-login btn btn-info btn-sm">Entrar</button>
          </div>

        </form>
    </div>
  </div>

<?php 
  $URI = preg_replace('/\/$|\/r2geotec\//i', '', $_SERVER['REQUEST_URI']);
  $inicio = empty($URI);
  $servicos = strpos($URI, 'servicos') !== FALSE;
  $admin = strpos($URI, 'area-adm') !== FALSE;
  $parceiros = $URI == 'parceiros';
  $portifolio = $URI == 'portifolio';
  $contato = $URI == 'contato';
?>

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <nav class="navbar navbar-default">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand logo-pequena-interno hidden-md hidden-lg" href="#">
              <img src="<?php echo base_url('publico/imagens/logo-r2-geotec-pequena.png'); ?>" >
            </a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <li class="<?php echo ($inicio ? ' item-ativo-menu' : '') ?>">
                  <a class="ajustes-menu-icones" href="<?php echo base_url(); ?>">
                    <img width="30px" src="<?php echo base_url('publico/imagens/icone-pagina-inicial.png'); ?>">

                    Página Inicial
                  </a>
                </li>
              </li>
              <li class="dropdown">
                <li class="<?php echo ($portifolio ? ' item-ativo-menu' : '') ?>">
                  <a class="ajustes-menu-icones" href="<?php echo base_url('portifolio'); ?>">
                    <img width="30px" src="<?php echo base_url('publico/imagens/icone-portifolio.png'); ?>">

                    Portifólio
                  </a>
                </li>
              </li>
              <li class="dropdown">
                <li class="<?php echo ($parceiros ? ' item-ativo-menu' : '') ?>">
                  <a class="ajustes-menu-icones" href="<?php echo base_url('parceiros'); ?>">
                  <img width="30px" src="<?php echo base_url('publico/imagens/icone-parceiros.png'); ?>">

                    Parceiros
                  </a>
                </li>
              </li>
              <li class="dropdown">
                <li class="<?php echo ($contato ? ' item-ativo-menu' : '') ?>">
                  <a class="ajustes-menu-icones" href="<?php echo base_url('contato'); ?>">
                  <img width="30px" src="<?php echo base_url('publico/imagens/icone-contato.png'); ?>">

                    Contato
                  </a>
                </li>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </div>
  </div>
  
  <div class="caixa-erro row">
  	<h1><?php echo $heading; ?></h1>
  	<span><?php echo $message; ?></span>
  </div>

</div>
	
	<footer>
		<div class="row footer-conteiner hidden-xs hidden-sm">
			<div class="logo-footer col-md-2 col-lg-2">
				<img src="<?php echo base_url('publico/imagens/logo-r2-geotec-pequena.png')?>">
			</div>
			<div class="contato-footer col-md-10 col-lg-10">
				<p>
					<b>R2 Geotec - </b>Rua José Cassimiro Filho, 498. Novo Planalto, Beberibe-Ceará.
				</p>

				<p>
					CEP: 62.840-000. Telefone: (85) 9 9915-4626. Whatsapp: (85) 9 8848-0470.
				</p>

				<p>
					© 2017. R2 Geotec. Todos os Direitos Reservados.
				</p>
			</div>
		</div>
	</footer>

	<div>
		<script src="<?php echo base_url('publico/js/libs/jquery-2.1.4.js')?>"></script>
		<script src="<?php echo base_url('publico/js/libs/bootstrap.min.js')?>"></script>
		<script src="<?php echo base_url('publico/js/geral.js')?>"></script>
		<script src="<?php echo base_url('publico/js/servicos.js')?>"></script>
	</div>
</body>
</html>