<div class="row">
  <div class="parceiros col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
    <div class="row">
      <div class="parceiros-titulo col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="text-center row">
          <h4 class="hidden-md hidden-lg"><b>Conheça Nossos Parceiros</b></h4>
          <h2 class="hidden-xs hidden-sm">Conheça Nossos Parceiros</h2>
        </div>
      </div>
    </div>

    <div class="row parceiros-celula">
      <div class="parceiros-celula col-xs-10 col-sm-10 col-md-4 col-lg-4 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
        <img src="<?php echo base_url('publico/imagens/.jpg'); ?>" width="100%">
      </div>

      <div class="parceiros-celula col-xs-10 col-sm-10 col-md-4 col-lg-4 col-xs-offset-1 col-sm-offset-1 col-md-offset-2 col-lg-offset-2">
        <img src="<?php echo base_url('publico/imagens/.jpg'); ?>" width="100%">
      </div>
    </div>
  </div>
</div>