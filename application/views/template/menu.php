<?php 
  $URI = preg_replace('/\/$|\/r2geotec\//i', '', $_SERVER['REQUEST_URI']);
  $inicio = empty($URI);
  $servicos = strpos($URI, 'servicos') !== FALSE;
  $admin = strpos($URI, 'area-adm') !== FALSE;
  $parceiros = $URI == 'parceiros';
  $portifolio = $URI == 'portifolio';
  $contato = $URI == 'contato';
?>

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <nav class="navbar navbar-default">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand logo-pequena-interno hidden-md hidden-lg" href="#">
            <img src="<?php echo base_url('publico/imagens/logo-r2-geotec-pequena.png'); ?>" >
          </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <li class="<?php echo ($inicio ? ' item-ativo-menu' : '') ?>">
                <a class="ajustes-menu-icones" href="<?php echo base_url(); ?>">
                  <img width="30px" src="<?php echo base_url('publico/imagens/icone-pagina-inicial.png'); ?>">

                  Página Inicial
                </a>
              </li>
            </li>
            <li class="dropdown <?php echo ($servicos ? ' item-ativo-menu' : '') ?>">
              <a href="#" class="dropdown-toggle ajustes-menu-icones" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <img width="30px" src="<?php echo base_url('publico/imagens/icone-servicos.png'); ?>">

                Serviços<span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li class="dropdown-submenu submenu-caixa">
                  <a class="test" tabindex="-1" href="#"><b>Topografia</b><span class="caret"></span></a>
                  <ul id="submenu-topografia" class="dropdown-menu">
                    <li><a tabindex="-1" href="<?php echo base_url('servicos/georeferenciamento'); ?>">Georeferenciamento</a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('servicos/loteamento'); ?>">Loteamento</a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('servicos/usucapiao'); ?>">Usucapião Urbano e Rural</a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('servicos/retificacao'); ?>">Retificação de Área</a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('servicos/desmembramento'); ?>">Desmembramento/Unificação</a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('servicos/curvas'); ?>">Curvas de Nível</a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('servicos/nossos_servicos'); ?>">Outros</a></li>
                  </ul>
                </li>

                <li class="dropdown-submenu">
                  <a class="test" tabindex="-1" href="#"><b>Licenciamento Ambiental</b><span class="caret"></span></a>
                  <ul id="submenu-licenciamento" class="dropdown-menu">
                    <li><a tabindex="-1" href="<?php echo base_url('servicos/licenciamento'); ?>">Licenciamento de Loteamentos</a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('servicos/empreendimentos'); ?>">Empreendimentos em Geral</a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('servicos/car'); ?>">Cadastro Ambiental Rural</a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('servicos/nossos_servicos'); ?>">Outros</a></li>
                  </ul>
                </li>
                
                <li class="dropdown-submenu">
                  <a class="test" tabindex="-1" href="#"><b>Projetos de Edificação</b><span class="caret"></span></a>
                  <ul id="submenu-edificacao" class="dropdown-menu">
                    <li><a tabindex="-1" href="<?php echo base_url('servicos/comerciais'); ?>">Projetos Comerciais</a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('servicos/residenciais'); ?>">Projetos Residênciais</a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('servicos/nossos_servicos'); ?>">Outros</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="dropdown">
              <li class="<?php echo ($portifolio ? ' item-ativo-menu' : '') ?>">
                <a class="ajustes-menu-icones" href="<?php echo base_url('portifolio'); ?>">
                  <img width="30px" src="<?php echo base_url('publico/imagens/icone-portifolio.png'); ?>">

                  Portifólio
                </a>
              </li>
            </li>
            <!--<li class="dropdown">
              <li class="<?php echo ($parceiros ? ' item-ativo-menu' : '') ?>">
                <a class="ajustes-menu-icones" href="<?php echo base_url('parceiros'); ?>">
                <img width="30px" src="<?php echo base_url('publico/imagens/icone-parceiros.png'); ?>">

                  Parceiros
                </a>
              </li>
            </li>-->
            <li class="dropdown">
              <li class="<?php echo ($contato ? ' item-ativo-menu' : '') ?>">
                <a class="ajustes-menu-icones" href="<?php echo base_url('contato'); ?>">
                <img width="30px" src="<?php echo base_url('publico/imagens/icone-contato.png'); ?>">

                  Contato
                </a>
              </li>
            </li>

            <?php if($this->session->userdata('logged_in')) { ?>
              <li class="dropdown">
                <li class="<?php echo ($admin ? ' item-ativo-menu' : '') ?>">
                  <a class="ajustes-menu-icones" href="<?php echo base_url('area-adm'); ?>">
                  <img width="30px" src="<?php echo base_url('publico/imagens/icone-perfil.png'); ?>">

                    Minha Conta
                  </a>
                </li>
              </li>
            <?php } ?>

          </ul>
        </div><!-- /.navbar-collapse -->

      </div><!-- /.container-fluid -->
    </nav>
  </div>
</div>