<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="icon" href="<?php echo base_url(); ?>publico/imagens/fiveicon.ico" type="image/x-icon">
  <link rel="shortcut icon" href="<?php echo base_url(); ?>publico/imagens/fiveicon.ico" type="image/x-icon">

  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>R2 Geotec</title>

  <!-- Bootstrap -->
  <?php echo link_tag('publico/css/libs/bootstrap.css'); ?>

  <!-- Geral -->
  <?php echo link_tag('publico/css/geral.css'); ?>

  <!-- Menu -->
  <?php echo link_tag('publico/css/template/menu.css'); ?>

  <!-- Header -->
  <?php echo link_tag('publico/css/template/header.css'); ?>

  <!-- Inicio -->
  <?php echo link_tag('publico/css/home.css'); ?>

  <!-- Portifolio -->
  <?php echo link_tag('publico/css/portifolio.css'); ?>

  <!-- Parceiros -->
  <?php echo link_tag('publico/css/parceiros.css'); ?>

  <!-- Contatos -->
  <?php echo link_tag('publico/css/contato.css'); ?>

  <!-- Logo -->
  <?php echo link_tag('publico/css/template/logo.css'); ?>

  <!-- Footer -->
  <?php echo link_tag('publico/css/template/footer.css'); ?>

  <!-- Servicos -->
  <?php echo link_tag('publico/css/servicos.css'); ?>

  <!-- Menu ADM -->
  <?php echo link_tag('publico/css/area_adm/ajustes.css'); ?>

  <!-- Chosen -->
  <?php echo link_tag('publico/js/libs/chosen/chosen.css'); ?>

  <!-- Datepicker -->
  <?php echo link_tag('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css'); ?>

  <!-- CSS Mobile -->
  <?php echo link_tag('publico/css/mobile/mobile-portrait.css'); ?>
  <?php echo link_tag('publico/css/mobile/mobile-landscape.css'); ?>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <div class="header col-xs-12 col-sm-12 col-md-12 col-lg-12" >
    <div class="hidden-xs hidden-sm col-md-4 col-lg-4 col-md-offset-1 col-lg-offset-1">
        <div class="logo-grande">
          <img src="<?php echo base_url('publico/imagens/logo-r2-geotec.png'); ?>" >
        </div>
    </div>

    <?php if(!$this->session->userdata('logged_in')) { ?>

      <div class="formulario-login hidden-xs hidden-sm col-md-6 col-lg-6">

          <form method="POST" action="<?php echo base_url('login'); ?>">
              <div class="form-group row">
                <div class="col-md-6 col-lg-6">
                  <label class="label-login" for="campo-usuario-1">Usuário: </label>
                </div>

                <div class="col-md-6 col-lg-6">
                  <div class="row">
                    <input type="text" class="form-control" id="campo-usuario-1" value="<?php echo set_value('usuario_login'); ?>" name="usuario_login" placeholder="Digite seu E-mail, CPF ou CNPJ">
                  </div>
                  <?php echo form_error('usuario_login'); ?>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-6 col-lg-6">
                  <label class="label-login" for="campo-senha-1">Senha: </label>
                </div>

                <div class="col-md-6 col-lg-6">
                  <div class="row">
                    <input type="password" class="form-control" id="campo-senha-1" value="<?php echo set_value('senha_login'); ?>" name="senha_login" placeholder="Digite sua Senha">
                  </div>
                  <?php echo form_error('senha_login'); ?>
                </div>
              </div>

            <div class="row">
              <button type="submit" class="botao-login btn btn-info btn-sm">Entrar</button>
            </div>

          </form>
      </div>
    <?php } else { ?>

        <div class="hidden-xs hidden-sm col-md-6 col-lg-6 text-right">
          <h4>
              Bem Vindo, Sr(a). <?php echo $this->session->userdata('usuario') ?>.
          </h4>
          
          <a href="<?php echo base_url('area-adm'); ?>"  class="botao-login btn btn-info btn-sm">Minha Conta</a>
          <a href="<?php echo base_url('logout'); ?>"  class="botao-login btn btn-danger btn-sm">Sair</a>
        </div>

    <?php } ?>
  </div>
