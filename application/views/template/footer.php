	</div>
	
	
	<footer>
		<div class="row footer-conteiner hidden-xs hidden-sm">
			<div class="logo-footer col-md-2 col-lg-2">
				<img src="<?php echo base_url('publico/imagens/logo-r2-geotec-pequena.png')?>">
			</div>
			<div class="contato-footer col-md-10 col-lg-10">
				<p>
					<b>R2 Geotec - </b>Rua José Cassimiro Filho, 498. Novo Planalto, Beberibe-Ceará.
				</p>

				<p>
					CEP: 62.840-000. Telefone: (85) 9 9915-4626. Whatsapp: (85) 9 8848-0470.
				</p>

				<p>
					© 2017. R2 Geotec. Todos os Direitos Reservados.
				</p>
			</div>
		</div>
	</footer>

	<div>
		<script src="<?php echo base_url('publico/js/libs/jquery-2.1.4.js')?>"></script>
		<script src="<?php echo base_url('publico/js/libs/bootstrap.min.js')?>"></script>
		<script src="<?php echo base_url('publico/js/libs/jquery.mask.js')?>"></script>
		<script src="<?php echo base_url('publico/js/mascaras.js')?>"></script>
		<script src="<?php echo base_url('publico/js/geral.js')?>"></script>
		<script src="<?php echo base_url('publico/js/servicos.js')?>"></script>
		<script src="<?php echo base_url('publico/js/portifolio.js')?>"></script>
		<script src="<?php echo base_url('publico/js/libs/chosen/chosen.jquery.min.js')?>"></script>
		<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDdFlRkOtESOdIGLJnnRaCBl0zTBwz6lns&amp;sensor=false"></script>
        <script src="<?php echo base_url('publico/js/mapa.js')?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
		<script>
		    $(document).ready(function(){
		      var date_input=$('#datetimepicker1');
		      var container=$('.data_container');
		      var options={
		        format: 'dd/mm/yyyy',
		        container: container,
		        todayHighlight: true,
		        autoclose: true,
		      };
		      date_input.datepicker(options);
		    })
		</script>
	</div>
</body>
</html>