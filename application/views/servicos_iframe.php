<!DOCTYPE html>
  <html lang="en">
    <head>
      <title>Bootstrap Example</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      
      <?php echo link_tag('publico/css/libs/bootstrap.css'); ?>
      <?php echo link_tag('publico/css/servicos_iframe.css'); ?>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      
    </head>
    <script> window.location.hash="<?php echo $servico; ?>";</script>

    <body data-spy="scroll" data-target="#myScrollspy" data-offset="20">
      <div class="container">
        <div class="row">
          <nav class="hidden-xs hidden-sm col-md-3 col-lg-3" id="myScrollspy">
            <ul class="nav nav-pills nav-stacked">
              <li class="dropdown active">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Topografia</b><span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#georeferenciamento">Georeferenciamento</a></li>
                  <li><a href="#loteamento">Loteamento</a></li>
                  <li><a href="#usucapiao">Usucapião Urbano e Rural</a></li>
                  <li><a href="#retificacao">Retificação de Área</a></li>
                  <li><a href="#desmembramento">Desmembramento/Unificação</a></li>
                  <li><a href="#curvas">Curvas de Nível</a></li>
                </ul>
              </li>

              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Licenciamento Ambiental</b><span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#licenciamento">Licenciamento de Loteamentos</a></li>
                  <li><a href="#empreendimentos">Empreendimentos em Geral</a></li>
                  <li><a href="#car">Cadastro Ambiental Rural</a></li>
                </ul>
              </li>
              
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Projetos de Edificação</b><span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#comerciais">Projetos Comerciais</a></li>
                  <li><a href="#residenciais">Projetos Residênciais</a></li>
                </ul>
              </li>
            </ul>
          </nav>

          <div class="col-sm-9">
            <div id="nossos_servicos" class="sessao-primeiro">
              <div class="row"> 
                <div class="col-sm-12 col-lg-12">  
                  
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                      <div class="item-servico-ajuste col-sm-11 col-lg-11 col-sm-offset-1 col-lg-offset-1">
                        <h4 class="hidden-md hidden-lg"><b>Nossos Serviços</b></h4>
                        <h2 class="hidden-xs hidden-sm">Nossos Serviços</h2>
                        <!--<p class="text-justify">
                          Sed ut perspiciatis unde omnis iste natus error sit
                          voluptatem accusantium doloremque laudantium, totam rem
                          aperiam, eaque ipsa quae ab illo inventore veritatis
                          et quasi architecto beatae vitae dicta sunt explicabo.
                        </p>-->
                      </div>
                    </div>
                    <!--<div class="hidden-xs hidden-sm col-sm-5 col-lg-5 imagem-servicos">
                      <img width="90%" src="<?php echo base_url('publico/imagens/servicos-1.jpg'); ?>"/>
                    </div>-->
                  </div>

                </div>
              </div>
            </div>

            <div id="georeferenciamento" class="sessao-par">
              <div class="row"> 
                <div class="col-sm-12 col-lg-12">  
                  
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                      <div class="item-servico-ajuste col-sm-11 col-lg-11 col-sm-offset-1 col-lg-offset-1">
                        <h4 class="hidden-md hidden-lg"><b>Georeferenciamento</b></h4>
                        <h3 class="hidden-xs hidden-sm">Georeferenciamento</h3>
                        <p class="text-justify">
                          <p>
                            Realizamos trabalhos de georreferenciamento de imóveis rurais atendendo à lei 10.267/01 e decretos 4.449/02 e 5.570/05,
                            bem como de imóveis urbanos.
                          </p>
                          <p>
                            A R2 Geotec, dispõe de equipamentos de alta precisão, como receptor geodésico L1L2,
                            oferecendo precisões milimétricas nas coordenadas.
                          </p>

                        </p>
                      </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-sm-5 col-lg-5 imagem-servicos">
                      <img width="90%" src="<?php echo base_url('publico/imagens/servicos-1.jpg'); ?>"/>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>

            <div id="loteamento" class="sessao-impar">
              <div class="row"> 
                <div class="col-sm-12 col-lg-12">  
                  
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                      <div class="item-servico-ajuste col-sm-11 col-lg-11 col-sm-offset-1 col-lg-offset-1">
                        <h4 class="hidden-md hidden-lg"><b>Loteamento</b></h4>
                        <h3 class="hidden-xs hidden-sm">Loteamento</h3>
                        <p class="text-justify">
                          <p>
                            A expansão das áreas urbanas e o parcelamento ordenado do solo,
                            fazem dos loteamentos, um empreendimento necessário para o desenvolvimento urbano dos municípios.
                            Sabemos que uma das etapas principais para implantação de um loteamento é a topografia. 
                          </p>
                          <p>
                            Desta forma, a R2 Geotec disponibiliza serviços topográficos que vão desde o
                            levantamento da área primitiva, até a urbanização do empreendimento.
                          </p>
                        </p>
                      </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-sm-5 col-lg-5 imagem-servicos">
                      <img width="90%" src="<?php echo base_url('publico/imagens/servicos-1.jpg'); ?>"/>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>

            <div id="usucapiao" class="sessao-par">
              <div class="row"> 
                <div class="col-sm-12 col-lg-12">  
                  
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                      <div class="item-servico-ajuste col-sm-11 col-lg-11 col-sm-offset-1 col-lg-offset-1">
                        <h4 class="hidden-md hidden-lg"><b>Usucapião Urbano e Rural</b></h4>
                        <h3 class="hidden-xs hidden-sm">Usucapião Urbano e Rural</h3>
                        <p class="text-justify">
                          <p>
                            Um dos principais instrumentos jurídicos de titulação de imóveis é a lei do usucapião.
                            Tanto imóveis rurais como imóveis urbanos podem ser objeto de ação de usucapião.
                          </p>
                          <p>
                            A R2 Geotec prepara as peças técnicas para composição do processo,
                            como memoriais descritivos, plantas, art junto ao CREA,
                            além de disponibilizar arquivos compartilhados com cartórios, prefeituras, despachantes, etc.
                          </p>
                        </p>
                      </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-sm-5 col-lg-5 imagem-servicos">
                      <img width="90%" src="<?php echo base_url('publico/imagens/servicos-1.jpg'); ?>"/>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>

            <div id="retificacao" class="sessao-impar">
              <div class="row"> 
                <div class="col-sm-12 col-lg-12">  
                  
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                      <div class="item-servico-ajuste col-sm-11 col-lg-11 col-sm-offset-1 col-lg-offset-1">
                        <h4 class="hidden-md hidden-lg"><b>Retificação de Área</b></h4>
                        <h3 class="hidden-xs hidden-sm">Retificação de Área</h3>
                        <p class="text-justify">
                          <p>
                            Atualmente, com equipamentos de alta tecnologia, a remedição dos imóveis revelam,
                            em sua grande maioria, discrepâncias nos cálculos dos perímetros e áreas.
                            Para corrigir e atualizar os dados de registros imobiliários
                            os proprietários podem lançar mão da retificação de área.
                          </p>
                          <p>
                            Para tanto, a realização de um levantamento criterioso d alta precisão é indispensável. 
                            A R2 Geotec, tem experiência no levantamento e produção de plantas atendendo às
                            exigências dos cartórios, facilitando o procedimento de retificação.
                          </p>
                        </p>
                      </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-sm-5 col-lg-5 imagem-servicos">
                      <img width="90%" src="<?php echo base_url('publico/imagens/servicos-1.jpg'); ?>"/>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>

            <div id="desmembramento" class="sessao-par">
              <div class="row"> 
                <div class="col-sm-12 col-lg-12">  
                  
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                      <div class="item-servico-ajuste col-sm-11 col-lg-11 col-sm-offset-1 col-lg-offset-1">
                        <h4 class="hidden-md hidden-lg"><b>Desmembramento e Unificação</b></h4>
                        <h3 class="hidden-xs hidden-sm">Desmembramento e Unificação</h3>
                        <p class="text-justify">
                          O desmembramento ou remembramento de matrículas é um procedimento muito utilizado por proprietários.
                          Tanto para imóveis rurais como urbanos,
                          a R2 Geotec realiza os serviços de preparação de documentos técnicos que serão submetidas aos cartórios,
                          prefeituras, INCRA, etc.
                        </p>
                      </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-sm-5 col-lg-5 imagem-servicos">
                      <img width="90%" src="<?php echo base_url('publico/imagens/servicos-1.jpg'); ?>"/>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>

            <div id="curvas" class="sessao-impar">
              <div class="row"> 
                <div class="col-sm-12 col-lg-12">  
                  
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                      <div class="item-servico-ajuste col-sm-11 col-lg-11 col-sm-offset-1 col-lg-offset-1">
                        <h4 class="hidden-md hidden-lg"><b>Curvas de Nível</b></h4>
                        <h3 class="hidden-xs hidden-sm">Curvas de Nível</h3>
                        <p class="text-justify">
                          <p>
                            Os levantamentos topográficos visando a projeção do relevo topográfico é conhecido
                            como levantamento altimétrico e sua projeção em planta chamada curvas de nível.
                            O uso deste tipo de levantamento e projeto é comum para imóveis próximos as áreas de marinha,
                            projetos agrícolas para implantação de irrigação, projetos de terraplenagem, estradas, etc.
                          </p>
                          <p>
                            A R2 Geotec dispões de níveis, estação total e receptor geodésico para realização de
                            levantamento e produção de plantas com curvas de níveis para as mais variadas finalidades.
                          </p>
                        </p>
                      </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-sm-5 col-lg-5 imagem-servicos">
                      <img width="90%" src="<?php echo base_url('publico/imagens/servicos-1.jpg'); ?>"/>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>

            <div id="licenciamento" class="sessao-par">
              <div class="row"> 
                <div class="col-sm-12 col-lg-12">  
                  
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                      <div class="item-servico-ajuste col-sm-11 col-lg-11 col-sm-offset-1 col-lg-offset-1">
                        <h4 class="hidden-md hidden-lg"><b>Licenciamento de Loteamentos</b></h4>
                        <h3 class="hidden-xs hidden-sm">Licenciamento de Loteamentos</h3>
                        <p class="text-justify">
                          Realizamos levantamentos, elaboramos plantas (planta baixa, de situação, etc.),
                          memoriais e arquivos (shape e kml ou kmz) voltados para licenciamento ambiental de empreendimentos imobiliários.
                        </p>
                      </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-sm-5 col-lg-5 imagem-servicos">
                      <img width="90%" src="<?php echo base_url('publico/imagens/servicos-1.jpg'); ?>"/>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>

            <div id="empreendimentos" class="sessao-impar">
              <div class="row"> 
                <div class="col-sm-12 col-lg-12">  
                  
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                      <div class="item-servico-ajuste col-sm-11 col-lg-11 col-sm-offset-1 col-lg-offset-1">
                        <h4 class="hidden-md hidden-lg"><b>Empreendimentos em Geral</b></h4>
                        <h3 class="hidden-xs hidden-sm">Empreendimentos em Geral</h3>
                        <p class="text-justify">
                          A R2 Geotec dispõem de serviços voltados para licenciamento
                          ambiental de empreendimentos em geral, como fabricas, comércios, pousadas, etc. 
                        </p>
                      </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-sm-5 col-lg-5 imagem-servicos">
                      <img width="90%" src="<?php echo base_url('publico/imagens/servicos-1.jpg'); ?>"/>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>

            <div id="car" class="sessao-par">
              <div class="row"> 
                <div class="col-sm-12 col-lg-12">  
                  
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                      <div class="item-servico-ajuste col-sm-11 col-lg-11 col-sm-offset-1 col-lg-offset-1">
                        <h4 class="hidden-md hidden-lg"><b>Cadastro Ambiental Rural</b></h4>
                        <h3 class="hidden-xs hidden-sm">Cadastro Ambiental Rural</h3>
                        <p class="text-justify">
                          O cadastro ambiental rural é obrigatório para todas as propriedades rurais do Brasil,
                          independente do tamanho ou título de domínio.
                          Para realizar o cadastro ambiental rural a R2 Geotec orienta e acompanha os produtores
                          rurais desde o levantamento de campo até o processo de preenchimento no SICAR.
                        </p>
                      </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-sm-5 col-lg-5 imagem-servicos">
                      <img width="90%" src="<?php echo base_url('publico/imagens/servicos-1.jpg'); ?>"/>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>

            <div id="comerciais" class="sessao-impar">
              <div class="row"> 
                <div class="col-sm-12 col-lg-12">  
                  
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                      <div class="item-servico-ajuste col-sm-11 col-lg-11 col-sm-offset-1 col-lg-offset-1">
                        <h4 class="hidden-md hidden-lg"><b>Projetos Comerciais</b></h4>
                        <h3 class="hidden-xs hidden-sm">Projetos Comerciais</h3>
                        <p class="text-justify">
                          Fazemos projetos de edificações de pequeno porte para fins comerciais.
                          Elaboramos as plantas, memoriais, art, etc. Tudo que seu projeto
                          precisa para aprovação e liberação de alvará.
                        </p>
                      </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-sm-5 col-lg-5 imagem-servicos">
                      <img width="90%" src="<?php echo base_url('publico/imagens/servicos-1.jpg'); ?>"/>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>

            <div id="residenciais" class="sessao-par">
              <div class="row"> 
                <div class="col-sm-12 col-lg-12">  
                  
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                      <div class="item-servico-ajuste col-sm-11 col-lg-11 col-sm-offset-1 col-lg-offset-1">
                        <h4 class="hidden-md hidden-lg"><b>Projetos Residênciais</b></h4>
                        <h3 class="hidden-xs hidden-sm">Projetos Residênciais</h3>
                        <p class="text-justify">
                          Fazemos projeto de edificações residenciais em geral. Elaboramos as plantas, memoriais, art, etc.
                          Tudo que seu projeto precisar para aprovação e liberação de alvará.
                        </p>
                      </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-sm-5 col-lg-5 imagem-servicos">
                      <img width="90%" src="<?php echo base_url('publico/imagens/servicos-1.jpg'); ?>"/>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </body>
  </html>