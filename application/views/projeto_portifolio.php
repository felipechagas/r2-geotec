<div class="row">
  <div class="projeto-portifolio col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
          <h4 class="hidden-md hidden-lg"><b><?php echo $titulo; ?></b></h4>
          <h2 class="text-left hidden-xs hidden-sm"><?php echo $titulo; ?></h2>
          <p class="text-justify">
            <?php echo $descricao; ?>
          </p>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <img src="<?php echo base_url('publico/imagens/projeto_'.$numero.'_1.jpg'); ?>" onclick="openModal(<?php echo $numero ?>);currentSlide(1, <?php echo $numero ?>)" class="hover-shadow">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <img src="<?php echo base_url('publico/imagens/projeto_'.$numero.'_2.jpg'); ?>" onclick="openModal(<?php echo $numero ?>);currentSlide(2, <?php echo $numero ?>)" class="hover-shadow">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <img src="<?php echo base_url('publico/imagens/projeto_'.$numero.'_3.jpg'); ?>" onclick="openModal(<?php echo $numero ?>);currentSlide(3, <?php echo $numero ?>)" class="hover-shadow">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <img src="<?php echo base_url('publico/imagens/projeto_'.$numero.'_4.jpg'); ?>" onclick="openModal(<?php echo $numero ?>);currentSlide(4, <?php echo $numero ?>)" class="hover-shadow">
      </div>
    </div>

    <div id="myModal<?php echo $numero ?>" class="modal" onclick="closeModal(<?php echo $numero ?>)">
      <span class="close cursor" onclick="closeModal(<?php echo $numero ?>)">&times;</span>
      <div class="modal-content" onclick="bloquear_click(<?php echo $numero ?>)">

        <div class="mySlides<?php echo $numero ?>">
          <div class="numbertext">1 / 4</div>
            <img src="<?php echo base_url('publico/imagens/projeto_'.$numero.'_1.jpg'); ?>" style="width:100%">
        </div>

        <div class="mySlides<?php echo $numero ?>">
          <div class="numbertext">2 / 4</div>
            <img src="<?php echo base_url('publico/imagens/projeto_'.$numero.'_2.jpg'); ?>" style="width:100%">
        </div>

        <div class="mySlides<?php echo $numero ?>">
          <div class="numbertext">3 / 4</div>
            <img src="<?php echo base_url('publico/imagens/projeto_'.$numero.'_3.jpg'); ?>" style="width:100%">
        </div>

        <div class="mySlides<?php echo $numero ?>">
          <div class="numbertext">4 / 4</div>
            <img src="<?php echo base_url('publico/imagens/projeto_'.$numero.'_4.jpg'); ?>" style="width:100%">
        </div>

        <a class="prev" onclick="plusSlides(-1, <?php echo $numero; ?>)">&#10094;</a>
        <a class="next" onclick="plusSlides(1, <?php echo $numero; ?>)">&#10095;</a>

        <div class="caption-container">
          <p id="caption<?php echo $numero; ?>"></p>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <img src="<?php echo base_url('publico/imagens/projeto_'.$numero.'_1.jpg'); ?>" onclick="currentSlide(1, <?php echo $numero ?>)" class="demo<?php echo $numero ?>">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <img src="<?php echo base_url('publico/imagens/projeto_'.$numero.'_2.jpg'); ?>" onclick="currentSlide(2, <?php echo $numero ?>)" class="demo<?php echo $numero ?>">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <img src="<?php echo base_url('publico/imagens/projeto_'.$numero.'_3.jpg'); ?>" onclick="currentSlide(3, <?php echo $numero ?>)" class="demo<?php echo $numero ?>">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <img src="<?php echo base_url('publico/imagens/projeto_'.$numero.'_4.jpg'); ?>" onclick="currentSlide(4, <?php echo $numero ?>)" class="demo<?php echo $numero ?>">
      </div>
    </div>
  </div>
</div>
</div>