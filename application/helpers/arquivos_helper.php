<?php

if ( ! function_exists('diretorio_absoluto'))
{
	function diretorio_absoluto($complemento=""){
		return ('C://xampp/htdocs/r2geotec/'.$complemento);
	}
}

if ( ! function_exists('pasta_upload'))
{
	function pasta_upload(){
		// CHAVE DE CRIPTOGRAFIA: RO77AL77AC

		return md5("RO77AL77AC");
	}
}

if ( ! function_exists('nome_arquivo_upload'))
{
	function nome_arquivo_upload($arquivo, $formato){
		return md5($arquivo.pasta_upload()).".".$formato;
	}
}

if ( ! function_exists('criar_pasta_upload'))
{
	function criar_pasta_upload($pasta, $subpasta){
		if(!file_exists(diretorio_absoluto(pasta_upload()."/".md5($pasta.pasta_upload())))) {
			$caminho = diretorio_absoluto(pasta_upload()."/".md5($pasta.pasta_upload()));
			mkdir($caminho);
		}

		$caminho = diretorio_absoluto(pasta_upload()."/".md5($pasta.pasta_upload())."/".md5($subpasta.pasta_upload()));
		mkdir($caminho);

		return $caminho;
	}
}

if ( ! function_exists('imagem_arquivo'))
{
	function imagem_arquivo($formato){
		switch ($formato) {
		    case "pdf":
		        return base_url('publico/imagens/icones_arquivos/pdf.png');

		    case "xls":
		        return base_url('publico/imagens/icones_arquivos/xls.png');

		    case "xlsx":
		        return base_url('publico/imagens/icones_arquivos/xls.png');

		    case "doc":
		        return base_url('publico/imagens/icones_arquivos/doc.png');

		    case "docx":
		        return base_url('publico/imagens/icones_arquivos/doc.png');

		    case "dwg":
		        return base_url('publico/imagens/icones_arquivos/dwg.png');

		    case "kml":
		        return base_url('publico/imagens/icones_arquivos/kml.png');

		    case "kmz":
		        return base_url('publico/imagens/icones_arquivos/kmz.png');

		    case "shp":
		        return base_url('publico/imagens/icones_arquivos/shp.png');

		    case "jpg":
		        return base_url('publico/imagens/icones_arquivos/jpg.png');

		    case "png":
		        return base_url('publico/imagens/icones_arquivos/png.png');

		    case "zip":
		        return base_url('publico/imagens/icones_arquivos/zip.png');

		    case "rar":
		        return base_url('publico/imagens/icones_arquivos/rar.png');

		    default:
		        return base_url('publico/imagens/icones_arquivos/file.png');
		}
	}
}

?>