<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* ----------------------------------------------------------------------------------------
   ----------------------------------------------------------------------------------------
                                        ENVIAR EMAIL
   ----------------------------------------------------------------------------------------
   ----------------------------------------------------------------------------------------
*/
function send_client_password($email_to = "", $new_password = "") {
  $subject = "R2Geotec Sistema - Mudança de Senha de Acesso";
  $message = "";
  //$message .= "<div style='text-align:left;font-size:22px;margin-top:20px;'><p>";
  //$message .= "Caro usuário do sistema R2Geo,";
  //$message .= "</p></div>";
  $message .= "<div style='text-align:center;font-size:22px;margin-top:20px;'><p>";
  $message .= "Você realizou seu cadastro com sucesso! Sua nova senha é : ";
  $message .= "<b>";
  $message .= $new_password;
  $message .= "</b>";
  $message .= ".";
  $message .= "</p>";
  $message .= "Para entrar no Sistema da R2Geotec utilize a nova senha. Você pode acessar o sistema clicando na logo.";
  $message .= "</div>";

  return send_email($subject, 
    CONS_CODEMAIL_FROM, 
    CONS_CODEMAIL_FROM_NAME, 
    CONS_CODEMAIL_USER_NAME, 
    CONS_CODEMAIL_PASSWORD,
    $email_to,
    $message
  );
}

function send_clients_report($email_to = "", $project_name = "", $report_message = "") {
  $subject = "R2Geotec Sistema - Relatório de Projeto";
  $message = "";
  $message .= "<div style='text-align:center;font-size:22px;margin-top:20px;'><p>";
  $message .= "Foi adicionado um novo relatório de atividade para o projeto <b>$project_name</b>.";
  $message .= "</p></div>";
  $message .= "</br>";
  $message .= "<table style='border-collapse:collapse;border-spacing:0;margin:0px auto;width:80%;'>";
  // CABECALHO
  $message .= "<tr>";
  $message .= "<th style='font-family:Arial, sans-serif;font-size:22px;font-weight:600;padding:10px 20px;border-style:solid;border-width:1px;border-color:#000;overflow:hidden;word-break:normal;background-color:#1083CC;color:#FFF;'>";
  $message .= "Projeto";
  $message .= "</th>";
  /*$message .= "<th style='font-family:Arial, sans-serif;font-size:22px;padding:10px 20px;border-style:solid;border-width:1px;border-color:#000;overflow:hidden;word-break:normal;background-color:#1083CC;color:#FFF;'>";
  $message .= "Atividade";
  $message .= "</th>";*/
  $message .= "<th style='font-family:Arial, sans-serif;font-size:22px;padding:10px 20px;border-style:solid;border-width:1px;border-color:#000;overflow:hidden;word-break:normal;background-color:#1083CC;color:#FFF;'>";
  $message .= "Data";
  $message .= "</th>";
  $message .= "</tr>";
  // COLUNA 1
  $message .= "<tr>";
  $message .= "<td style='font-family:Arial, sans-serif;font-size:22px;font-weight:600;padding:10px 20px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;'>";
  $message .= "$project_name";
  $message .= "</td>";
  // COLUNA 2
  /*$message .= "<td style='font-family:Arial, sans-serif;font-size:22px;padding:10px 20px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;'>";
  $message .= "$report_message";
  $message .= "</td>";*/
  // COLUNA 3
  $today = date("d/m/Y H:i");
  $message .= "<td style='font-family:Arial, sans-serif;font-size:22px;padding:10px 20px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;'>";
  $message .= $today;
  $message .= "</td>";
  $message .= "</tr>";

  $message .= "</table>";

  return send_email($subject, 
    CONS_CODEMAIL_FROM, 
    CONS_CODEMAIL_FROM_NAME, 
    CONS_CODEMAIL_USER_NAME, 
    CONS_CODEMAIL_PASSWORD,
    $email_to,
    $message
  );
}

function send_email($subject = "", $email_from = "", $name_from = "", $user = "", $pass = "", $email_to = "", $message = "") {
  try {
    if (empty($message) || empty($email_to) || empty($name_from)) {
      return FALSE;
    }

    $config = Array(
      'protocol' => CONS_CODEMAIL_PROTOCOL,
      'smtp_host' => CONS_CODEMAIL_HOST,
      'smtp_port' => CONS_CODEMAIL_PORT,
      'smtp_user' => $user,
      'smtp_pass' => $pass,
      //'smtp_crypto' => "tls",
      'charset' => CONS_CODEMAIL_CHARSET,
      'wordwrap' => TRUE
    );

    $logo_file = CONS_CODEMAIL_IMG_PATH;

    $CI =& get_instance();
    $CI->load->library('email', $config);

    $CI->email->attach($logo_file, "inline");

    $header = "";
    $header .= "<div style='text-align:left;margin-top:20px;'>";
    $header .= "<a href='" . base_url() . "'>";
    $header .= "<img style='width:200px;' src='cid:" . $CI->email->attachment_cid($logo_file) . "' border='0'/>";
    $header .= "</a>";
    $header .= "<hr style='margin: 15px 0 5px 0;height:6px;border: 0;background:#1083CC;'/>";
    $header .= "</div>";

    $CI->email->set_newline("\r\n");
    $CI->email->from($email_from, $name_from);
    $CI->email->to($email_to);
    $CI->email->subject($subject);
    $CI->email->message($header . $message);
    $CI->email->set_mailtype(CONS_CODEMAIL_TYPE);

    if (!$CI->email->send()) {
      show_error("Ocorreu um erro ao enviar o e-mail. Verifique se todas as informações de e-mail estão corretas.");
      //show_error($CI->email->print_debugger());
    } else {
      return TRUE;
    }
  } catch (Exception $e) {
    log('error', 'ERRO HELPER - SEND EMAIL - ' . $e->getMessage());

    return FALSE;
  }
  return TRUE;
}

/*function send_emails($subject = "", $email_from = "", $name_from = "", $user = "", $pass = "", $emails_to = "", $message = "") {
  try {
    if (empty($message) || empty($email_to) || empty($name_from)) {
      return FALSE;
    }

    $config = Array(
      'protocol' => CONS_CODEMAIL_PROTOCOL,
      'smtp_host' => CONS_CODEMAIL_HOST,
      'smtp_port' => CONS_CODEMAIL_PORT,
      'smtp_user' => $user,
      'smtp_pass' => $pass,
      //'smtp_crypto' => "tls",
      'charset' => CONS_CODEMAIL_CHARSET,
      'wordwrap' => TRUE
    );

    $logo_file = CONS_CODEMAIL_IMG_PATH;

    $CI =& get_instance();
    $CI->load->library('email', $config);

    $CI->email->attach($logo_file, "inline");

    $header = "";
    $header .= "<div style='text-align:center;margin-top:20px;'>";
    $header .= "<a href='" . base_url() . "'>";
    $header .= "<img src='cid:" . $CI->email->attachment_cid($logo_file) . "' border='0'/>";
    $header .= "</a>";
    $header .= "<hr style='' />";
    $header .= "</div>";

    $message .= "<div style='text-align:center;margin-top:20px;'>";
    $message .= "<a href='" . base_url() . "'>";
    $message .= "<img src='cid:" . $CI->email->attachment_cid($logo_file) . "' border='0'/>";
    $message .= "</a>";
    $message .= "</div>";

    $CI->email->set_newline("\r\n");
    $CI->email->from($email_from, $name_from);
    $CI->email->to($email_to);
    $CI->email->subject($subject);
    $CI->email->message($message);
    $CI->email->set_mailtype(CONS_CODEMAIL_TYPE);
    

    if (!$CI->email->send()) {
      show_error($CI->email->print_debugger());
    } else {
      return TRUE;
    }
  } catch (Exception $e) {
    log('error', 'ERRO HELPER - SEND EMAIL - ' . $e->getMessage());

    return FALSE;
  }
  return TRUE;
}*/

/*function enviar_email($assunto, $mensagem = '', $email = '', $nome = '', $email_destino = '',$replyTo = '', $replyToName = '') {
  try {
    if (empty($mensagem) || empty($email) || empty($nome)) {
      return FALSE;
    }

    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->Host       = CONS_EMAIL_HOST;        // Specify main and backup SMTP servers
    $mail->SMTPAuth   = CONS_EMAIL_SMPT_AUTH;   // Enable SMTP authentication
    $mail->Username   = CONS_EMAIL_USER_NAME;   // SMTP username
    $mail->Password   = CONS_EMAIL_PASSWORD;    // SMTP password
    $mail->SMTPSecure = CONS_EMAIL_SMPT_SECURE; // Enable TLS encryption, `ssl` also accepted
    $mail->Port       = CONS_EMAIL_PORT;        // TCP port to connect to
    $mail->From       = CONS_EMAIL_FROM;
    $mail->FromName   = CONS_EMAIL_FROM_NAME;
    $mail->addAddress($email_destino, $nome);
    if (!empty($replyTo) && !empty($replyToName)) {
      $mail->addReplyTo($replyTo, $replyToName);
    }
    else
    {
      $mail->addReplyTo(CONS_EMAIL_REPLY_TO, CONS_EMAIL_REPLY_NAME);
    }
    $mail->isHTML(CONS_EMAIL_IS_HTML);
    $mail->CharSet    = CONS_EMAIL_CHARSET;
    $mail->Subject    = $assunto;
    $mail->AltBody    = 'Para visualizar a mensagem corretamente, utilize um leitor de e-mail compatível com HTML';
    $mail->AddEmbeddedImage($caminho_imagens . 'email-logo-citinova.jpg', 'logo-citinova', 'email-logo-citinova.jpg');
    $mail->AddEmbeddedImage($caminho_imagens . 'email-logo-hacker-cidadao.jpg', 'logo-hacker-cidadao', 'email-logo-hacker-cidadao.jpg');
    $mail->Body       = $mensagem;

    if(!$mail->send()) {
        echo "DEU MERDA";
        die();

        return FALSE;
    } 
  } catch (phpmailerException $e) {
    echo $e->errorMessage();

    die();
  } catch (Exception $e) {
    log('error', 'ERRO HELPER - EMAIL - ' . $e->getMessage());

    return FALSE;
  }
  return TRUE;
}*/
