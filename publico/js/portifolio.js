function openModal(numero_projeto) {
  document.getElementById('myModal'+numero_projeto).style.display = "block";
}

function closeModal(numero_projeto) {
  document.getElementById('myModal'+numero_projeto).style.display = "none";
}

function bloquear_click(numero_projeto) {
  event.stopPropagation();
}

var slideIndex = [1, 1, 1];

showSlides(slideIndex[0], 1);
showSlides(slideIndex[1], 2);
showSlides(slideIndex[2], 3);

function plusSlides(n, numero_projeto) {
  showSlides(slideIndex[numero_projeto-1] += n, numero_projeto);
}

function currentSlide(n, numero_projeto) {
  showSlides(slideIndex[numero_projeto-1] = n, numero_projeto);
}

function showSlides(n, numero_projeto) {
  var i;
  var slides = document.getElementsByClassName("mySlides"+numero_projeto);

  console.log();

  var dots = document.getElementsByClassName("demo"+numero_projeto);
  var captionText = document.getElementById("caption"+numero_projeto);
  if (n > slides.length) {slideIndex[numero_projeto-1] = 1}
  if (n < 1) {slideIndex[numero_projeto-1] = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }

  slides[slideIndex[numero_projeto-1]-1].style.display = "block";
  dots[slideIndex[numero_projeto-1]-1].className += " active";
  captionText.innerHTML = dots[slideIndex[numero_projeto-1]-1].alt;
}