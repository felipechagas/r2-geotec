var map;
 
function initialize() {
    var latlng = new google.maps.LatLng(-4.1884696, -38.1227381);
 
    var options = {
        zoom: 15,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
 
    map = new google.maps.Map(document.getElementById("mapa"), options);
}
 
initialize();

function carregarPontos() {
 
    $.getJSON(base_url('publico/js/pontos.json'), function(pontos) {
 
        $.each(pontos, function(index, ponto) {
 
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(ponto.Latitude, ponto.Longitude),
                title: "R2 Geotec",
                map: map,
                icon: base_url('publico/imagens/marker_r2geotec.png')
            });
 
        });
 
    });
 
}
 
carregarPontos();