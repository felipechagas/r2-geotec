function base_url() {
    return "http://localhost/r2geotec/"
    //return "http://r2geotec.com.br/"
}

function base_url(url) {
    return "http://localhost/r2geotec/"+url;
    //return "http://r2geotec.com.br/"+url;
}

function adaptar_altura_inf_home(altura) {
  $("div.altura-adaptador").each(function() {
    $(this).css("height", altura);
  });
}

/*function get_div_mais_alta() {
  //Só falta fazer o algoritimo pra pegar a maior div(mais alta)
  var divs_home = $("div.altura-adaptador").toArray();
  
  for (var i = 0; i < divs_home.length; i++) {
    $(divs_home[i]).css("height");
  };
}*/

$(document).ready(function(){

  $("#clientes_adicionais").hide();

  $("#btn_clientes_adicionais").on("click", function(e){
    $("#clientes_adicionais").show();
    $("#btn_clientes_adicionais").hide();
  });

  $('.dropdown-submenu a.test').on("click", function(e){
    $('#submenu-topografia').hide();
    $('#submenu-licenciamento').hide();
    $('#submenu-edificacao').hide();

    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });

  $(".chosen-select").chosen({
    disable_search_threshold: 5,
    no_results_text: "Nenhum resultado encontrado.",
    width: "100%"
  });


  //get_div_mais_alta();

  var altura = $("#mais-alta").css("height");
  adaptar_altura_inf_home(altura);

  $( window ).resize(function() {
    var altura2 = $("#mais-alta").css("height");
    console.log(altura);
    adaptar_altura_inf_home(altura);
  });

});